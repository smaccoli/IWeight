#ifndef TOOLS_H
#define TOOLS_H
#include <TMath.h>
#include <map>
#include <TString.h>
using namespace std;

// -----------------------------------------
// Particle MC Codes
// -----------------------------------------
namespace PdgCode
{
  int Gamma= 22;
  int E    = 11;
  int NuE  = 12;
  int Mu   = 13;
  int NuMu = 14;
  int Tau  = 15;
  int NuTau= 16;
  int K    = 321;
  int Pi   = 211;
  int Pi0  = 111;
  int D0   = 421;
  int D    = 411;
  int DS   = 413;
  int Ds   = 431;
  int DsS  = 433;
  int P    = 2212;
  int Phi  = 333;
  int B    = 521;
  int B0   = 511;
  int Bs   = 531;
  int Bc   = 541;
  int K0   = 311;
  int K0s  = 310;
  int K0l  = 130;
  int Lm   = 3122;
  int Lc   = 4122;
  int Lb   = 5122;
}


namespace PdgMass
{
  double mKS0_PDG     = 497.611;
  double mKplus_PDG   = 493.677;
  double mPiplus_PDG  = 139.57061;
  double mproton_PDG  = 938.272081;
  double mLc_PDG      = 2286.46;
  double mL0_PDG      = 1115.683;
  double mD0_PDG      = 1864.84;
  double mDp      = 1869.61;
}


namespace AxisTitle
{
  map<TString, TString> m = {
    //D+
    {"Dplus_P","p (D^{+}) [MeV/c]"},
    {"Dplus_PT","p_{T} (D^{+}) [MeV/c]"},
    {"Dplus_ETA","#eta (D^{+})"},
    {"Dplus_PHI","#varphi (D^{+})"},
    {"Dplus_PX","p_{X} (D^{+}) [MeV/c]"},
    {"Dplus_PY","p_{Y} (D^{+}) [MeV/c]"},
    {"Dplus_PZ","p_{Z} (D^{+}) [MeV/c]"},
    //h+
    {"hplus_P","p (h^{+}) [MeV/c]"},
    {"hplus_PT","p_{T} (h^{+}) [MeV/c]"},
    {"hplus_ETA","#eta (h^{+})"},
    {"hplus_PHI","#varphi (h^{+})"},
    {"hplus_PX","p_{X} (h^{+}) [MeV/c]"},
    {"hplus_PY","p_{Y} (h^{+}) [MeV/c]"},
    {"hplus_PZ","p_{Z} (h^{+}) [MeV/c]"},
    //pi+
    {"piplus_P","p (#pi^{+}) [MeV/c]"},
    {"piplus_PT","p_{T} (#pi^{+}) [MeV/c]"},
    {"piplus_ETA","#eta (#pi^{+})"},
    {"piplus_PHI","#varphi (#pi^{+})"},
    {"piplus_PX","p_{X} (#pi^{+}) [MeV/c]"},
    {"piplus_PY","p_{Y} (#pi^{+}) [MeV/c]"},
    {"piplus_PZ","p_{Z} (#pi^{+}) [MeV/c]"},
    //K-
    {"Kminus_P","p (K^{#minus}) [MeV/c]"},
    {"Kminus_PT","p_{T} (K^{#minus}) [MeV/c]"},
    {"Kminus_ETA","#eta (K^{#minus})"},
    {"Kminus_PHI","#varphi (K^{#minus})"},
    {"Kminus_PX","p_{X} (K^{#minus}) [MeV/c]"},
    {"Kminus_PY","p_{Y} (K^{#minus}) [MeV/c]"},
    {"Kminus_PZ","p_{Z} (K^{#minus}) [MeV/c]"},
   
    //p1 = pi+
    {"P1_P","p (#pi^{+}) [MeV/c]"},
    {"P1_PT","p_{T} (#pi^{+}) [MeV/c]"},
    {"P1_ETA","#eta (#pi^{+})"},
    {"P1_PHI","#varphi (#pi^{+})"},
    //p2 = K-
    {"P2_P","p (K^{#minus}) [MeV/c]"},
    {"P2_PT","p_{T} (K^{#minus}) [MeV/c]"},
    {"P2_ETA","#eta (K^{#minus})"},
    {"P2_PHI","#varphi (K^{#minus})"},
    
    //X
    {"X_P","p (X) [MeV/c]"},
    {"X_PT","p_{T} (X) [MeV/c]"},
    {"X_ETA","#eta (X)"},
    {"X_PXI","#pXi (X)"},
    {"X_PX","p_{X} (X) [MeV/c]"},
    {"X_PY","p_{Y} (X) [MeV/c]"},
    {"X_PZ","p_{Z} (X) [MeV/c]"},
    //soft pi+
    {"sPi_P","p (#pi_{soft}^{+}) [MeV/c]"},
    {"sPi_PT","p_{T} (#pi_{soft}^{+}) [MeV/c]"},
    {"sPi_ETA","#eta (#pi_{soft}^{+})"},
    {"sPi_PHI","#varphi (#pi_{soft}^{+})"},
    {"sPi_PX","p_{X} (#pi_{soft}^{+}) [MeV/c]"},
    {"sPi_PY","p_{Y} (#pi_{soft}^{+}) [MeV/c]"},
    {"sPi_PZ","p_{Z} (#pi_{soft}^{+}) [MeV/c]"},
    //D*+
    {"Dst_P","p (D^{*+}) [MeV/c]"},
    {"Dst_PT","p_{T} (D^{*+}) [MeV/c]"},
    {"Dst_ETA","#eta (D^{*+})"},
    {"Dst_PHI","#varphi (D^{*+})"},
    {"Dst_PX","p_{X} (D^{*+}) [MeV/c]"},
    {"Dst_PY","p_{Y} (D^{*+}) [MeV/c]"},
    {"Dst_PZ","p_{Z} (D^{*+}) [MeV/c]"},
    //D0
    {"D0_P","p (D^{0}) [MeV/c]"},
    {"D0_PT","p_{T} (D^{0}) [MeV/c]"},
    {"D0_ETA","#eta (D^{0})"},
    {"D0_PHI","#varphi (D^{0})"},
    {"D0_PX","p_{X} (D^{0}) [MeV/c]"},
    {"D0_PY","p_{Y} (D^{0}) [MeV/c]"},
    {"D0_PZ","p_{Z} (D^{0}) [MeV/c]"},
    //Ds+
    {"Dsplus_P","p (D_{s}^{+}) [MeV/c]"},
    {"Dsplus_PT","p_{T} (D_{s}^{+}) [MeV/c]"},
    {"Dsplus_ETA","#eta (D_{s}^{+})"},
    {"Dsplus_PHI","#varphi (D_{s}^{+})"},
    {"Dsplus_PX","p_{X} (D_{s}^{+}) [MeV/c]"},
    {"Dsplus_PY","p_{Y} (D_{s}^{+}) [MeV/c]"},
    {"Dsplus_PZ","p_{Z} (D_{s}^{+}) [MeV/c]"}
   
  };
}

TString findAxisTitle(TString name) {
  TString title = AxisTitle::m[name];
  if (title == "")
    return name;
  else
    return title;
}


namespace DecayTitle
{
  map<TString, TString> m = {
    {"D2KSpi","D^{+} #rightarrow K_{S}^{0} #pi^{+}"},
    {"D2Kpipi","D^{+} #rightarrow K^{#minus} #pi^{+} #pi^{+}"},
    {"D2KSh","D^{+} #rightarrow K_{S}^{0} h^{+}"},
    {"D2Kpih","D^{+} #rightarrow K^{#minus} #pi^{+} h^{+}"},
    {"D02Kpi","D^{0} #rightarrow K^{#minus} #pi^{+}"},
    {"Dp2KS0pipLL","D^{+} #rightarrow K_{S}^{0} #pi^{+}"},
    {"Dp2Kmpippip","D^{+} #rightarrow K^{#minus} #pi^{+} #pi^{+}"},
    {"D02Kmpip","D^{0} #rightarrow K^{#minus} #pi^{+}"},
    {"D02KmKp","D^{0} #rightarrow K^{#minus} K^{+}"}
  };
}

TString findDecayTitle(TString name) {
  TString title = DecayTitle::m[name];
  if (title == "")
    return name;
  else
    return title;
}

string to_string(TString word) {
  const char* a =   word;
  string b = a;
  return b;
}


void getAsym(double Nplus, double Nminus, double Nplus_err = 0, double Nminus_err = 0, bool use_Nerr2 = 0) {

  if (use_Nerr2) {
    Nplus_err = sqrt(Nplus_err);
    Nminus_err = sqrt(Nminus_err);
  }

  if (Nplus_err == 0)
    Nplus_err = sqrt(Nplus);
  if (Nminus_err == 0)
    Nminus_err = sqrt(Nminus);


  double asymmetry = (Nplus-Nminus)/(Nplus+Nminus);
  
  double asymmetry_err = sqrt(
			      pow(2*Nminus/(Nplus+Nminus)/(Nplus+Nminus),2) * pow(Nplus_err,2) +
			      pow(2*Nplus/(Nplus+Nminus)/(Nplus+Nminus),2) * pow(Nminus_err,2)
			      );

  cout << asymmetry << " pm " << asymmetry_err << endl;
 
}


#endif // TOOLS_H

