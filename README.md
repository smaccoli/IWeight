# IWeight

Fast (c++) tool for iterative and multidimensional reweighting. TTree* objects needed as input.
Tested with ROOT/6.14.04

Reweighting is the procedure of finding such weights for original (toweight) sample,
that make distribution of one or several variables identical to the one of the target sample.

Typical application of this technique in HEP is reweighting of Monte-Carlo simulation
results to minimize disagreement between simulated data and real data. 
Frequently the reweighting rule is trained on one part of data (normalization channel)
and applied to different (signal channel).

The tool is born to be used in CP violation studies where nuisance asymmetries are deleted through kinematic weighting of control channels.
Here particle distribution (pt,eta,phi) must agree between signal and control channel, i.e. 3 dimensions needed for each particle.


IWeight is extremely flexible, it support:
 - N-dimensional distributions,
 - more than one target sample,
 - iterative weighting on more distributions,
 - updating of the weights applied to the target (in case it is also used as toweight sample),
 - customization such as setting a maximum value for the weights and applying fiducial cuts
 (empty regions of the toweight sample are automatically removed on the target sample),
 - smoothing algorithms but not recommended for N > 2.

Other useful features are:
 - before and after plot of the variables,
 - estimate of the asymmetry arising from imperfect agreement of two distributions.

Remark: if each variable has identical distribution in two samples, 
this doesn’t imply that multidimensional distributions are equal (almost surely they aren’t). 
Therefore, it is not recommended to apply iterative weighting on variables of the same particles, unless they have zero correlation or the two samples share the same 3D space.

