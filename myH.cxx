#include "myH.h"
#include <ctime>
#include "smoothAlgorithm_6D.cxx"
//#include "smoothAlgorithm_6D_new.cxx"

myH::myH(vector < size_t > Nbins, vector < double > xmins, vector < double > xmaxs) : Nbins(Nbins), xmins(xmins), xmaxs(xmaxs) {
  //check sizes...

  size_t N = 1;
 
  Ndims = Nbins.size();
  for (size_t i_dim = 0; i_dim < Ndims; i_dim++) {
    dxs.push_back((xmaxs.at(i_dim) - xmins.at(i_dim) )/Nbins.at(i_dim));
    N *= Nbins.at(i_dim);
  }

  Nbins_tot = N;

  for (size_t i_X = 0; i_X < Nbins_tot; i_X++) {
    BinContent.push_back(0);
    BinError.push_back(0);
  }
    
}

 
size_t myH::find(vector < double > xs, bool create_mapOfBins = 0) {

  vector < size_t > i_Xs;
  size_t i_X = 0;
  size_t N = 1;
  for (size_t i_dim = 0 ; i_dim < Ndims; i_dim++) {
    i_Xs.push_back((size_t)(( xs.at(i_dim) - xmins.at(i_dim) )/dxs.at(i_dim)));
    if (xs.at(i_dim) < xmins.at(i_dim) || xs.at(i_dim) >= xmaxs.at(i_dim)) {
      //cout << "outerbin FIND" << endl;
      return Nbins_tot;
    }
  }
  
  for (int i_dim = Ndims-1 ; i_dim >= 0; i_dim--) {
    i_X +=  N*i_Xs.at(i_dim);
    N *= Nbins.at(i_dim);
  }
  
  if (create_mapOfBins) {
    //   if (mapOfBins.find(i_X) == mapOfBins.end())
    mapOfBins[i_X] = i_Xs;
    
  }

  return i_X;
}

void myH::fill(vector < double > xs) {
  size_t i_X = find(xs);
  if (i_X < Nbins_tot) {
    BinContent[i_X] += 1; 
    BinError[i_X] = sqrt(BinContent.at(i_X));
  }
}

void myH::fill(vector < double > xs, float w) {
  size_t i_X = find(xs);
  if (i_X < Nbins_tot) {
    if (w == w) { 
      BinContent[i_X] += w;
      BinError[i_X] = sqrt(BinError.at(i_X)*BinError.at(i_X) + w*w); //sumw2
      if (BinError.at(i_X) != BinError.at(i_X))
	BinError[i_X] = 0;
    }
  }
  else {
    //cout << "outerbin FILL" << endl;
  }/*
     if (BinError.at(i_X) != BinError.at(i_X)) {
     cout << "here, weight = ";
     cout << w << endl;
     }
     if (getBE(i_X) != getBE(i_X)) {
     cout << "here, weight = ";
     cout << w << endl;
     }
   */
}

float myH::getBC(size_t i_X) {
  float bc;
  if (i_X < Nbins_tot) {
    bc = BinContent.at(i_X);
    if (bc == bc)
      return bc;
  }
  return 0;
}

void myH::setBC(size_t i_X, float bc) {
  if (i_X < Nbins_tot) {  
    BinContent[i_X] = bc; 
    BinError[i_X] = sqrt(bc);
  }  
}

float myH::getBE(size_t i_X) {
  float be;
  if (i_X < Nbins_tot) {
    be = BinError.at(i_X);
    if (be == be)
      return be;
  }
  return 0;
}

void myH::setBE(size_t i_X, float be) {
  if (i_X < Nbins_tot)  
    BinError[i_X] = be; 
}

void myH::add(myH *h, float factor, bool calculateError) {
  for (size_t i_X = 0; i_X < Nbins_tot; i_X++) {
    BinContent[i_X] = BinContent.at(i_X)+factor*h->BinContent.at(i_X);
  }
  if (calculateError)
    for (size_t i_X = 0; i_X < Nbins_tot; i_X++) {
      BinError[i_X] = sqrt(BinError.at(i_X)*BinError.at(i_X)+factor*factor*h->BinError.at(i_X)*h->BinError.at(i_X));
    }
}

void myH::add_constant(float constant) {
  for (size_t i_X = 0; i_X < Nbins_tot; i_X++) {
    BinContent[i_X] = BinContent.at(i_X)+constant;
  }
}

void myH::getasymmetry(myH *h, bool calculateError) {
  if (calculateError)
    for (size_t i_X = 0; i_X < Nbins_tot; i_X++) {
      BinError[i_X] = sqrt( 4*pow(h->BinContent.at(i_X),2)/pow(BinContent.at(i_X) + h->BinContent.at(i_X),4)*pow(BinError.at(i_X),2) + 4*pow(BinContent.at(i_X),2)/pow(BinContent.at(i_X) + h->BinContent.at(i_X),4)*pow(h->BinError.at(i_X),2) );
    }
  
  for (size_t i_X = 0; i_X < Nbins_tot; i_X++) {
    BinContent[i_X] = (BinContent.at(i_X) - h->BinContent.at(i_X)) / (BinContent.at(i_X) + h->BinContent.at(i_X));
  }

}


void myH::divide(myH *h, bool calculateError) {
  for (size_t i_X = 0; i_X < Nbins_tot; i_X++) {
    if (h->BinContent.at(i_X) != 0)
      BinContent[i_X] = BinContent.at(i_X)/h->BinContent.at(i_X);
    else {
      BinContent[i_X] = 0;
    }
  }
  //missing
}

void myH::multiply(myH *h, bool calculateError) {
  if (calculateError)
    for (size_t i_X = 0; i_X < Nbins_tot; i_X++) {
      BinError[i_X] = sqrt(pow(h->BinContent.at(i_X),2)*pow(BinError.at(i_X),2) + pow(BinContent.at(i_X),2)*pow(h->BinError.at(i_X),2));
      if (BinError.at(i_X) != BinError.at(i_X))
	BinError[i_X] = 0;
    }
  for (size_t i_X = 0; i_X < Nbins_tot; i_X++) {
    BinContent[i_X] = BinContent.at(i_X)*h->BinContent.at(i_X);
    if (BinContent.at(i_X) != BinContent.at(i_X))
      BinContent[i_X] = 0;
  }
 
}

double myH::integral(bool calculateError) {
  double I = 0;
  for (size_t i_X = 0; i_X < Nbins_tot; i_X++) {
    I += BinContent.at(i_X);
  }
  double Ie = 0;
  if (calculateError) {
    for (size_t i_X = 0; i_X < Nbins_tot; i_X++) {
      Ie += BinError.at(i_X)*BinError.at(i_X);
    }
    Ie = sqrt(Ie);
    cout << I << " \\pm  " << Ie << endl;
  }
  
  return I;
}

double myH::integral(double& Ierr) {
  double I = 0;
  for (size_t i_X = 0; i_X < Nbins_tot; i_X++) {
    I += BinContent.at(i_X);
  }
  double Ie = 0;
  for (size_t i_X = 0; i_X < Nbins_tot; i_X++) {
    Ie += BinError.at(i_X)*BinError.at(i_X);
  }
  Ie = sqrt(Ie);
  cout << I << " \\pm  " << Ie << endl;

  Ierr = Ie;

  return I;
}

void myH::scale(float factor) {
  for (size_t i_X = 0; i_X < Nbins_tot; i_X++) {
    BinContent[i_X] = BinContent.at(i_X)*factor;
    BinError[i_X] = BinError.at(i_X)*fabs(factor);
  }
}

void myH::norm() {
  double I = integral(/*bool calculateError = */0);
  if (I != 0)
    scale(1./I);
}

void myH::dividenorm(myH *h,bool calculateError) {
  double I = integral(0);

  for (size_t i_X = 0; i_X < Nbins_tot; i_X++) {
    if (h->BinContent.at(i_X) != 0 && BinContent.at(i_X) != 0)
      BinContent[i_X] = BinContent.at(i_X)/h->BinContent.at(i_X);
    else 
      BinContent[i_X] = 0;   
  }

  if(calculateError) {
    for (size_t i_X = 0; i_X < Nbins_tot; i_X++) {
      if (h->BinContent.at(i_X) != 0 && BinContent.at(i_X) != 0)
	//BinContent[i_X] is already changed
	BinError[i_X] = sqrt(pow(1./h->BinContent.at(i_X),2)*pow(BinError.at(i_X),2) + pow(BinContent.at(i_X)/h->BinContent.at(i_X),2)*pow(h->BinError.at(i_X),2));
      else 
	BinError[i_X] = 0;   
    }    
  }
    
  
  if (I != 0)
    scale(h->integral()/I);
  
}

double myH::getChi2(myH *h) {
  double chi2 = 0;
  double tmp = 0;
  double test = 0;
  double scale_factor = h->integral(/*bool calculateError = */0)/integral(/*bool calculateError = */0);
  
  for(size_t i_X = 1; i_X < Nbins_tot; i_X++) {
    if ((pow(h->getBE(i_X),2)+pow(scale_factor*getBE(i_X),2)) != 0) {
      tmp = pow(h->getBC(i_X) - scale_factor*getBC(i_X),2)/(pow(h->getBE(i_X),2)+pow(scale_factor*getBE(i_X),2));
      if (tmp == tmp)
	chi2 += tmp;
    }

    if ((/*pow(h->getBE(i_X),2)+*/pow(getBE(i_X),2)) != 0)
      test += pow(h->getBC(i_X) - scale_factor*getBC(i_X),2)/(/*pow(h->getBE(i_X),2)+*/pow(scale_factor*getBE(i_X),2));

    if (chi2 != chi2)
      cout << h->getBC(i_X) << " -  " << scale_factor*getBC(i_X) << " /  " << pow(h->getBE(i_X),2)+pow(scale_factor*getBE(i_X),2) << " if  " << pow(h->getBE(i_X),2)+pow(getBE(i_X),2) <<" A= " << h->getBE(i_X) << " B=  " << getBE(i_X) << endl;
    
  }
  //cout << test << endl;
  return chi2;
}

void myH::positive() {
  float BCcut = 0;
  float sum_negative = 0;
  float sum_positive = 0;
  //counting negative bin contents
  for (size_t i_X = 0; i_X < Nbins_tot; i_X++) {
    if (BinContent[i_X] < BCcut) {
      sum_negative += - BinContent[i_X];
      BinContent[i_X] = 0;
      BinError[i_X] = 0;
    }
  }
  
  //counting positive bin contentes under a certain threshold
  /*
    if (sum_negative > 1e-2)
    while(1) {
    BCcut += 1e-2;
    for (size_t i_X = 0; i_X < Nbins_tot; i_X++) {
    if (BinContent[i_X] < BCcut) {
    sum_positive += BinContent[i_X];
    BinContent[i_X] = 0;
    BinError[i_X] = 0;
    }
    }
    if (sum_positive >= sum_negative) {
    break;
    }
    }
  */
  //cout << "sum_positive sum_negative BCcut" << endl;
  //cout << sum_positive << " " << sum_negative << " " << BCcut << endl;

}

void myH::zero() {
  for (size_t i_X = 0; i_X < Nbins_tot; i_X++) {
    BinContent[i_X] = 0;
    BinError[i_X] = 0;
  }
}
void myH::zeroErrors() {
  for (size_t i_X = 0; i_X < Nbins_tot; i_X++) {
    BinError[i_X] = 0;
  }
}

void myH::leveling(float BCmax/*Iw_max*/,float BCmax_value/*Iw_max_value*/) {
  for (size_t i_X = 0; i_X < Nbins_tot; i_X++) {
    if (BinContent[i_X] > BCmax) {
      if (BinContent[i_X]!=0)
	BinError[i_X] = BinError[i_X]*BCmax_value/BinContent[i_X];
      BinContent[i_X] = BCmax_value;
    }
  }
}


TH3D myH::convert2TH3D() {

  size_t NbinsX = Nbins.at(0);
  size_t NbinsY = Nbins.at(1);
  size_t NbinsZ = Nbins.at(2);
  TH3D h("h","h",NbinsX,xmins.at(0),xmaxs.at(0),NbinsY,xmins.at(1),xmaxs.at(1),NbinsZ,xmins.at(2),xmaxs.at(2));

  size_t i_XYZ;
  for (size_t i_X = 1; i_X <= NbinsX; i_X++) {
    for (size_t i_Y = 1; i_Y <= NbinsY; i_Y++) {
      for (size_t i_Z = 1; i_Z <= NbinsZ; i_Z++) {
	i_XYZ = NbinsZ*NbinsY*(i_X-1) + NbinsZ*(i_Y-1) + (i_Z-1);
	h.SetBinContent(i_X,i_Y,i_Z,getBC(i_XYZ));
	h.SetBinError(i_X,i_Y,i_Z,getBE(i_XYZ));
      }
    } 
  }

  return h;
}

TH2D myH::convert2TH2D() {

  size_t NbinsX = Nbins.at(0);
  size_t NbinsY = Nbins.at(1);
  TH2D h("h","h",NbinsX,xmins.at(0),xmaxs.at(0),NbinsY,xmins.at(1),xmaxs.at(1));

  size_t i_XY;
  for (size_t i_X = 1; i_X <= NbinsX; i_X++) {
    for (size_t i_Y = 1; i_Y <= NbinsY; i_Y++) {
      i_XY = NbinsY*(i_X-1) + (i_Y-1);
      h.SetBinContent(i_X,i_Y,getBC(i_XY));
      h.SetBinError(i_X,i_Y,getBE(i_XY));
    }
  }

  return h;
}

TH1D myH::convert2TH1D() {

  size_t NbinsX = Nbins.at(0);
  TH1D h("h","h",NbinsX,xmins.at(0),xmaxs.at(0));

  for (size_t i_X = 1; i_X <= NbinsX; i_X++) {
    h.SetBinContent(i_X,getBC(i_X-1));
    h.SetBinError(i_X,getBE(i_X-1));
  }

  return h;
}

TH1D myH::convert2TH1D(size_t i_dim) {

  size_t NbinsX = Nbins.at(0);
  size_t NbinsY = 1;
  size_t NbinsZ = 1;
  size_t Nbins4 = 1;
  size_t Nbins5 = 1;
  size_t Nbins6 = 1;
  if (Nbins.size()>=2)
    NbinsY = Nbins.at(1);
  if (Nbins.size()>=3)
    NbinsZ = Nbins.at(2);
  if (Nbins.size()>=4)
    Nbins4 = Nbins.at(3);
  if (Nbins.size()>=5)
    Nbins5 = Nbins.at(4);
  if (Nbins.size()>=6)
    Nbins6 = Nbins.at(5);
  
  TH1D h("h","h",Nbins.at(i_dim),xmins.at(i_dim),xmaxs.at(i_dim));
  double sum[Nbins.at(i_dim)];
  double esum2[Nbins.at(i_dim)];
  for (size_t i = 0; i < Nbins.at(i_dim); i++) {
    sum[i] = 0;
    esum2[i] = 0;
  }
  
  size_t i_XY;
  for (size_t i_X = 0; i_X < NbinsX; i_X++) {
    for (size_t i_Y = 0; i_Y < NbinsY; i_Y++) {
      for (size_t i_Z = 0; i_Z < NbinsZ; i_Z++) {
	for (size_t i_4 = 0; i_4 < Nbins4; i_4++) {
	  for (size_t i_5 = 0; i_5 < Nbins5; i_5++) {
	    for (size_t i_6 = 0; i_6 < Nbins6; i_6++) {
	      
	      i_XY = NbinsY*NbinsZ*Nbins4*Nbins5*Nbins6*(i_X) + NbinsZ*Nbins4*Nbins5*Nbins6*(i_Y) + Nbins4*Nbins5*Nbins6*(i_Z) + Nbins5*Nbins6*(i_4) + Nbins6*(i_5) + i_6;
	      
	      if (i_dim == 0) {
		sum[i_X] += getBC(i_XY);
		esum2[i_X] += pow(getBE(i_XY),2);
	      }
	      else if (i_dim == 1) {
		sum[i_Y] += getBC(i_XY);
		esum2[i_Y] += pow(getBE(i_XY),2);
	      }
	      else if (i_dim == 2) {
		sum[i_Z] += getBC(i_XY);
		esum2[i_Z] += pow(getBE(i_XY),2);
	      }
	      else if (i_dim == 3) {
		sum[i_4] += getBC(i_XY);
		esum2[i_4] += pow(getBE(i_XY),2);
	      }
	      else if (i_dim == 4) {
		sum[i_5] += getBC(i_XY);
		esum2[i_5] += pow(getBE(i_XY),2);
	      }
	      else if (i_dim == 5) {
		sum[i_6] += getBC(i_XY);
		esum2[i_6] += pow(getBE(i_XY),2);
	      }
	
	    }
	  }
	}
      }
    }
  }

  for (size_t i = 1; i <= Nbins.at(i_dim); i++) {	    
    h.SetBinContent(i,sum[i-1]);
    h.SetBinError(i,sqrt(esum2[i-1]));
  }
	    
  return h;
}

TH1D myH::getBCdistribution(vector < size_t> EmptyBins) {
  vector < float > v;
  v.reserve(Nbins_tot);
  size_t k = 0;
  for (size_t i = 0; i < Nbins_tot; i++) {
    if ( k < EmptyBins.size()) {
      if (i != EmptyBins.at(k))
	v.push_back(getBC(i));
      else
	k++;
    }
    else
      v.push_back(getBC(i));
      
  }
  sort(v.begin(), v.end());
  TH1D h("h","h",(int)v.at(v.size()-1)+1,0,(int)v.at(v.size()-1)+1);
  for (size_t i = 0; i < v.size(); i++) {
    h.Fill(v.at(i));
  }
  return h;
}


void myH::smoothApply(vector < vector < size_t > > multiple_bins) {

  double bc;
  size_t Nbins;
  size_t i_XY;
  size_t Nmultiplebins = multiple_bins.size();

  for (size_t i = 0; i < Nmultiplebins; i++) {
    bc = 0;
    Nbins = multiple_bins.at(i).size();
    for (size_t j = 0; j < Nbins; j++) {//Nbins per set
      /*
	if (this->Nbins.size()==3)
	i_XY = this->Nbins.at(2)*this->Nbins.at(1)*multiple_bins.at(i).at(j).at(0)+this->Nbins.at(2)*multiple_bins.at(i).at(j).at(1)+multiple_bins.at(i).at(j).at(2);
	else
	i_XY = this->Nbins.at(1)*multiple_bins.at(i).at(j).at(0)+multiple_bins.at(i).at(j).at(1);
      */
      i_XY = multiple_bins.at(i).at(j);
      bc += getBC(i_XY);
    }
    for (size_t j = 0; j < Nbins; j++) {//Nbins per set
      /*
	if (this->Nbins.size()==3)
	i_XY = this->Nbins.at(2)*this->Nbins.at(1)*multiple_bins.at(i).at(j).at(0)+this->Nbins.at(2)*multiple_bins.at(i).at(j).at(1)+multiple_bins.at(i).at(j).at(2);
	else
	i_XY = this->Nbins.at(1)*multiple_bins.at(i).at(j).at(0)+multiple_bins.at(i).at(j).at(1);
      */
      i_XY = multiple_bins.at(i).at(j);  
      setBC(i_XY, bc/Nbins);
    }
  }
  
}

vector < size_t > myH::commonEmptyBins(myH *h) {
 
  vector < size_t > EmptyBins;
  
  for (size_t i_X = 0; i_X < Nbins_tot; i_X++) {
    if (h->BinContent[i_X] == 0 && BinContent[i_X] == 0)
      EmptyBins.push_back(i_X);
  }
  
  return EmptyBins;
}

void myH::writeToFile(string fname = "tmp.myH") {
  ofstream f;
  f.open(fname,ios::binary);
  f.write((char*) &Ndims, sizeof(Ndims));
  f.write((char*) &Nbins_tot, sizeof(Nbins_tot));
 
  for (size_t i_dim = 0; i_dim < Ndims; i_dim++) {
    f.write((char*) &Nbins.at(i_dim), sizeof(Nbins.at(0)));
    f.write((char*) &xmins.at(i_dim), sizeof(xmins.at(0)));
    f.write((char*) &xmaxs.at(i_dim), sizeof(xmaxs.at(0)));
  }
  
  for (size_t i_X = 0; i_X < Nbins_tot; i_X++) {
    f.write((char*) &BinContent.at(i_X), sizeof(BinContent.at(0)));
    f.write((char*) &BinError.at(i_X), sizeof(BinError.at(0)));
  }

  f.close();
}

void myH::readFromFile(string fname = "tmp.myH") {
  ifstream f;
  f.open (fname,ios::binary);
  f.seekg(0,ios::end);
  int size = f.tellg();
  f.seekg(0,ios::beg);
  
  Nbins.clear();
  xmins.clear();
  xmaxs.clear();
  BinContent.clear();
  BinError.clear();
  
  size_t N = 0;
  double x = 0;
  float y = 0;
  f.read((char*) &N, sizeof(N));
  Ndims = N;
  f.read((char*) &N, sizeof(N));
  Nbins_tot = N;
  while (f.tellg() < size) {
    if (Nbins.size() < Ndims) { 
      f.read((char*) &N, sizeof(N));
      Nbins.push_back(N);
      f.read((char*) &x, sizeof(x));
      xmins.push_back(x);
      f.read((char*) &x, sizeof(x));
      xmaxs.push_back(x);
    }
    else {
      f.read((char*) &y, sizeof(y));
      BinContent.push_back(y);   
      f.read((char*) &y, sizeof(y));
      BinError.push_back(y);   
    }
    
  }

  for (size_t i_dim = 0; i_dim < Ndims; i_dim++) {
    dxs.push_back((xmaxs.at(i_dim) - xmins.at(i_dim) )/Nbins.at(i_dim));
  }

  if (BinContent.size() != Nbins_tot || BinError.size() != Nbins_tot)
    cout << "reading error!!" << endl;
  
 
  f.close();
}

myH::myH(string fname) {
  readFromFile(fname);    
}

myH::myH(TString fname) {
  readFromFile(to_string(fname));    
}


myH::myH() {
}



/* 
   #include "TApplication.h"

   int main(int argc, char **argv) {
   TApplication *myapp= new TApplication("myapp",&argc, argv);
  
    
   myH* h = new myH({100,100},{3e3,1.5},{15e3,4.7});
   for (int i = 0; i< 1000; i++) {
   h->fill({4e3,3});
   h->fill({3.0001e3,1.5001});
   }
   cout << h->find({3.0001e3,1.5001}) << endl;
   cout << h->getBC(h->find({3.0001e3,1.5001})) << endl;
   cout << h->getBE(h->find({3.0001e3,1.5001})) << endl;
   TH1D h1d =  h->getBCdistribution({});
   h1d.Draw();
   cout << h1d.GetXaxis()->GetXmax() << endl;
   cout << h1d.GetNbinsX() << endl; 
   cout << h1d.GetBinContent(99) << endl;
   cout << h1d.GetBinContent(100) << endl;
   cout << h1d.GetBinContent(101) << endl;
   myapp->Run();
   return 0;
   }

*/
