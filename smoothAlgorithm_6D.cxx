
vector < vector < size_t > > myH::smoothAlgorithm(float BCmin, vector < size_t> EmptyBins) {
  //  cout << BCmin << endl;
  
  float I = integral();
  float bcmin = I;
  size_t bcmin_i_X, bcmin_i_Y, bcmin_i_Z;
  size_t bcmin_i_4, bcmin_i_5, bcmin_i_6;
  float bc;
  float close_bcmin = I;
  //float close_bcmax = -I;
  size_t close_bcmin_i_X, close_bcmin_i_Y, close_bcmin_i_Z;
  size_t close_bcmin_i_4, close_bcmin_i_5, close_bcmin_i_6;
  float close_bc;

  vector < vector <  size_t > > multiple_bins;
  
  size_t bcmin_i;
  size_t close_bcmin_i;;
  size_t Nmultibins, Nbins_A, Nbins_B;
  bool done;

  myH  tmp_h = *this;
  myH  multiplebins_h = *this;
  multiplebins_h.zero();
  myH  commonEB_h = *this;
  commonEB_h.zero();
  
  vector < pair < float, vector <size_t> > >  lowbc_bins;
  
  size_t NbinsX = Nbins.at(0);
  size_t NbinsY = Nbins.at(1);
  size_t NbinsZ = 1;
  size_t Nbins4 = 1;
  size_t Nbins5 = 1;
  size_t Nbins6 = 1;
  if (Nbins.size()>=3)
    NbinsZ = Nbins.at(2);
  if (Nbins.size()>=4)
    Nbins4 = Nbins.at(3);
  if (Nbins.size()>=5)
    Nbins5 = Nbins.at(4);
  if (Nbins.size()>=6)
    Nbins6 = Nbins.at(5);
   
  size_t i_XY;
  size_t i_commonEB = 0;
  bool commonEB = 0;
  
  bool bcmin_already_merged, close_bcmin_already_merged;
  size_t EBsize = EmptyBins.size();

  //cout << EBsize << endl;

  i_commonEB = 0;
  lowbc_bins.clear();
      
  for (size_t i_X = 0; i_X < NbinsX; i_X++) {
    for (size_t i_Y = 0; i_Y < NbinsY; i_Y++) {
      for (size_t i_Z = 0; i_Z < NbinsZ; i_Z++) {
	for (size_t i_4 = 0; i_4 < Nbins4; i_4++) {
	  for (size_t i_5 = 0; i_5 < Nbins5; i_5++) {
	    for (size_t i_6 = 0; i_6 < Nbins6; i_6++) {
	      commonEB = 0;
	      i_XY = NbinsY*NbinsZ*Nbins4*Nbins5*Nbins6*(i_X) + NbinsZ*Nbins4*Nbins5*Nbins6*(i_Y) + Nbins4*Nbins5*Nbins6*(i_Z) + Nbins5*Nbins6*(i_4) + Nbins6*(i_5) + i_6;
	      // i_XY = NbinsZ*NbinsY*(i_X) + NbinsZ*(i_Y) + i_Z;
	      //i_XY = NbinsY*(i_X) + (i_Y);
	
	      if (i_commonEB < EBsize)
		if (i_XY == EmptyBins.at(i_commonEB)) {
		  commonEB_h.setBC(i_XY,1);
		  commonEB = 1;
		  //cout << i_XY << endl;
		  i_commonEB++;
		}

	      bc = tmp_h.getBC(i_XY); 
	      if (bc <= (BCmin)*2 && !commonEB)
		lowbc_bins.push_back({bc,{i_X,i_Y,i_Z,i_4,i_5,i_6}});
	    }
	  }
	}
      }
    }
  }

  sort(lowbc_bins.begin(), lowbc_bins.end());

  // size_t i_A_old = 0;
  // size_t i_B_old = 0;
  // bool i_A_old_used = 0;
  // bool i_B_old_used = 0;

  size_t i = 0;
  size_t lowbc_bins_size = lowbc_bins.size();

  cout << "lowbc_bins_size  " << lowbc_bins_size << endl;

  while(i < lowbc_bins_size) {
    bc = -I;
    done = 0;
    bcmin = I+1;
    close_bcmin = I+1;

    //find get bin with lowest yields: (bcmin)
    //bcmin = lowbc_bins.at(i).first;
    bcmin_i_X = lowbc_bins.at(i).second.at(0);//first;
    bcmin_i_Y = lowbc_bins.at(i).second.at(1);//second;
    bcmin_i_Z = lowbc_bins.at(i).second.at(2);//third
    bcmin_i_4 = lowbc_bins.at(i).second.at(3);
    bcmin_i_5 = lowbc_bins.at(i).second.at(4);
    bcmin_i_6 = lowbc_bins.at(i).second.at(5);
    i_XY = NbinsY*NbinsZ*Nbins4*Nbins5*Nbins6*(bcmin_i_X) + NbinsZ*Nbins4*Nbins5*Nbins6*(bcmin_i_Y) + Nbins4*Nbins5*Nbins6*(bcmin_i_Z) + Nbins5*Nbins6*(bcmin_i_4) + Nbins6*(bcmin_i_5) + bcmin_i_6;
    //i_XY = NbinsZ*NbinsY*(bcmin_i_X) + NbinsZ*(bcmin_i_Y) + (bcmin_i_Z);
    //i_XY = NbinsY*(bcmin_i_X) + (bcmin_i_Y);
    bcmin = tmp_h.getBC(i_XY);
      
    i++;
    
    if(bcmin > BCmin)
      continue;  
 
    //cout << bcmin_i_X << '\t' << bcmin_i_Y << '\t' << bcmin << endl;

    //find close bin with lowest yield
    // close_lowbc_bins.clear();
    for(int i_X = ((int)bcmin_i_X) - 1; i_X <= ((int)bcmin_i_X) + 1; i_X++) {
      for(int i_Y = ((int)bcmin_i_Y) -1; i_Y <= ((int)bcmin_i_Y) + 1; i_Y++) {
	for(int i_Z = ((int)bcmin_i_Z) -1; i_Z <= ((int)bcmin_i_Z) + 1; i_Z++) {
	  for(int i_4 = ((int)bcmin_i_4) - 1; i_4 <= ((int)bcmin_i_4) + 1; i_4++) {
	    for(int i_5 = ((int)bcmin_i_5) -1; i_5 <= ((int)bcmin_i_5) + 1; i_5++) {
	      for(int i_6 = ((int)bcmin_i_6) -1; i_6 <= ((int)bcmin_i_6) + 1; i_6++) {

		if (
		    i_X < 0 || i_Y < 0 || i_Z < 0 || i_4 < 0 || i_5 < 0 || i_6 < 0 || 
		    i_X >= NbinsX || i_Y >= NbinsY || i_Z >= NbinsZ ||  i_4 >= Nbins4 || i_5 >= Nbins5 || i_6 >= Nbins6 || 
		    (i_X == bcmin_i_X && i_Y == bcmin_i_Y && i_Z == bcmin_i_Z && i_4 == bcmin_i_4 && i_5 == bcmin_i_5 && i_6 == bcmin_i_6)
		    )
		  continue;
		//no diagonal bins
		/*
		if (!(i_X == bcmin_i_X || i_Y == bcmin_i_Y || i_Z == bcmin_i_Z))
		  continue;
		*/
		i_XY = NbinsY*NbinsZ*Nbins4*Nbins5*Nbins6*(i_X) + NbinsZ*Nbins4*Nbins5*Nbins6*(i_Y) + Nbins4*Nbins5*Nbins6*(i_Z) + Nbins5*Nbins6*(i_4) + Nbins6*(i_5) + i_6;
		//i_XY = NbinsZ*NbinsY*(i_X) + NbinsZ*(i_Y)+i_Z;
		//i_XY = NbinsY*(i_X) + (i_Y);
		close_bc = tmp_h.getBC(i_XY);
	    
		//cout <<"bc = " << i_X << '\t' << i_Y << '\t' << close_bc << endl;
	    
	    
		// close_lowbc_bins.push_back({close_bc,{i_X,i_Y,i_Z}});

		if( close_bc < close_bcmin && !commonEB_h.getBC(i_XY)) {
		  close_bcmin = close_bc;
		  close_bcmin_i_X = i_X;
		  close_bcmin_i_Y = i_Y;
		  close_bcmin_i_Z = i_Z;
		  close_bcmin_i_4 = i_4;
		  close_bcmin_i_5 = i_5;
		  close_bcmin_i_6 = i_6;
		}
		/*
		if( close_bc > close_bcmax) {
		  close_bcmax = close_bc;
		}
		*/
	      }
	    }
	  }
	}
      }
    }

    /*
      if(bcmin >= BCmin && close_bcmin >= BCmin) {
      cout << bcmin << "  " << close_bcmin << endl;
      continue;  
      } 
    */
    
    //cout << close_bcmin_i_X << '\t' << close_bcmin_i_Y << '\t' << close_bcmin << endl;
    i_XY = NbinsY*NbinsZ*Nbins4*Nbins5*Nbins6*(bcmin_i_X) + NbinsZ*Nbins4*Nbins5*Nbins6*(bcmin_i_Y) + Nbins4*Nbins5*Nbins6*(bcmin_i_Z) + Nbins5*Nbins6*(bcmin_i_4) + Nbins6*(bcmin_i_5) + bcmin_i_6;
    //i_XY = NbinsZ*NbinsY*(bcmin_i_X) + NbinsZ*(bcmin_i_Y) + (bcmin_i_Z);
    //i_XY = NbinsY*(bcmin_i_X) + (bcmin_i_Y);
    //tmp_h.setBC(i_XY,I);
    //tmp_h.setBC(i_XY,close_bcmax);
    tmp_h.setBC(i_XY,(bcmin+close_bcmin)/2);
    bcmin_i = i_XY;//{bcmin_i_X,bcmin_i_Y,bcmin_i_Z};
    bcmin_already_merged = multiplebins_h.getBC(i_XY);
    multiplebins_h.setBC(i_XY,1);
    i_XY = NbinsY*NbinsZ*Nbins4*Nbins5*Nbins6*(close_bcmin_i_X) + NbinsZ*Nbins4*Nbins5*Nbins6*(close_bcmin_i_Y) + Nbins4*Nbins5*Nbins6*(close_bcmin_i_Z) + Nbins5*Nbins6*(close_bcmin_i_4) + Nbins6*(close_bcmin_i_5) + close_bcmin_i_6;
    //i_XY = NbinsZ*NbinsY*(close_bcmin_i_X) + NbinsZ*(close_bcmin_i_Y) + (close_bcmin_i_Z);
    //i_XY = NbinsY*(close_bcmin_i_X) + (close_bcmin_i_Y);
    tmp_h.setBC(i_XY,(bcmin+close_bcmin)/2);
    close_bcmin_i = i_XY;//{close_bcmin_i_X,close_bcmin_i_Y,close_bcmin_i_Z};
    close_bcmin_already_merged = multiplebins_h.getBC(i_XY);
    multiplebins_h.setBC(i_XY,1);
      
    // cout << bcmin_already_merged << "  " << close_bcmin_already_merged << endl;
     
    //merge of two multibins
    if(!done && bcmin_already_merged && close_bcmin_already_merged) {
      // i_A_old_used = 0;      
      // for (int i_A = i_A_old; i_A < multiple_bins.size() && done == 0; i_A++) {
      for (int i_A = ((int)multiple_bins.size()) - 1; i_A >= 0  && done == 0; i_A--) {
	Nbins_A = multiple_bins.at(i_A).size();
	for (size_t j_A = 0; j_A < Nbins_A; j_A++) {
	  if (multiple_bins.at(i_A).at(j_A) == bcmin_i) {
	    // i_B_old_used = 0;
	    // for (int i_B = i_B_old; i_B < multiple_bins.size() && done == 0; i_B++) {
	    for (int i_B = ((int)multiple_bins.size()) - 1; i_B >= 0  && done == 0; i_B--) {
	      Nbins_B = multiple_bins.at(i_B).size();
	      // cout << "i_A" << " " << "i_B";
	      // cout << " / i_A_old" << " " << "i_B_old";
	      // cout << " / i_A_old_used" << " " << "i_B_old_used" << endl;
	      // cout << i_A << " " << i_B << " / ";
	      // cout << i_A_old << " " << i_B_old << " / ";
	      // cout << i_A_old_used << " " << i_B_old_used << endl;
	      //cout << i_A << " " << i_B << endl;
	      for (size_t j_B = 0; j_B < Nbins_B; j_B++) {
		if (multiple_bins.at(i_B).at(j_B) == close_bcmin_i) {
		  if (i_A != i_B) {
		    for (size_t k_B = 0; k_B < Nbins_B; k_B++) {
		      multiple_bins.at(i_A).push_back(multiple_bins.at(i_B).at(k_B));
		    }
		    multiple_bins.at(i_B).clear();
		    Nbins_B = 0;
		  }
		  else {
		    //the bins already belong to the same multibin
		  }
		  done = 1;
		  // i_A_old = i_A;
		  // i_B_old = i_B;
		  //cout << "done" << endl;
		}
	      }
	      // if(i_B_old_used == 0 && i_B + 1  == multiple_bins.size()) {
	      // 	i_B = -1; 
	      // 	i_B_old_used = 1;
	      // }
	    }
	  }
	}
	// if(i_A_old_used == 0 && i_A + 1  ==  multiple_bins.size()) {
	//   i_A = -1; 
	//   i_A_old_used = 1;
	// }
      }
    }						
   
    //one of the two bins altready in a multibin
    if(!done && (bcmin_already_merged || close_bcmin_already_merged) ) {
      // i_A_old_used = 0;
      // i_B_old_used = 0;
      // for (int i = i_A_old; i < multiple_bins.size() && done == 0; i++) {
      for (int i = ((int)multiple_bins.size()) - 1 ; i >= 0 && done == 0; i--) {
	Nmultibins = multiple_bins.at(i).size();
	// cout << "i" << " / i_A_old " << "i_B_old" << " / i_A_old_used " << "i_B_old_used" << endl;
	// cout << i << " /  " << i_A_old << " " << i_B_old << " /  " << i_A_old_used << " " << i_B_old_used << endl;
	//cout << i << endl;
	for (size_t j = 0; j < Nmultibins && done == 0; j++) {
	  if (multiple_bins.at(i).at(j) == bcmin_i) {
	    multiple_bins.at(i).push_back(close_bcmin_i);
	    done = 1;
	    //cout << "done" << endl;
	    // i_A_old = i;	 
	    // cout << "done i_A_old = " << i << endl;
	  }
	  else if (multiple_bins.at(i).at(j) == close_bcmin_i) {
	    multiple_bins.at(i).push_back(bcmin_i);
	    done = 1;
	    //cout << "done" << endl;
	    // i_B_old = i;	 
	    // cout << "done i_B_old = " << i << endl;
	  }
	}



	// if(i_A_old_used == 0) {
	//   if (i_A_old != i_B_old)
	//     i = ((int)i_B_old) - 1;
	//   else if (i_A_old != 0)
	//     i = -1;
	//   i_A_old_used = 1;
	// }
	// else if(i + 1 == i_A_old) {
	//   i = i_A_old; //skip i_A_old
	// }
	// if(i_B_old_used == 0 && i + 1  >=  multiple_bins.size()) {
	//   i = -1;
	//   i_B_old_used = 1;	  
	// }
	// if(i_A_old_used && i + 1 == i_A_old) {
	//   i = i_A_old; //skip i_A_old
	// }
 
      }
    }
    

    if (!done) {
      multiple_bins.push_back({bcmin_i,close_bcmin_i});
      //cout << "end" << endl;
    }
  }

  cout << "Binning optimized with BinContent > " << BCmin;
  cout << ". Nmultiplebins = " << multiple_bins.size();
  cout << endl;


  return multiple_bins;
}
