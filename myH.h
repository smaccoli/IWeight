#include <math.h>
#include <vector>
#include <map>
#include <iostream>
#include <iomanip>
#include <fstream>
#include "TFile.h"
#include "TH3D.h"
#include "TH2D.h"
#include "TH1D.h"
#include <algorithm>
#include "Tools.h"

using namespace std;

class myH {
public:

  size_t Ndims;
  size_t Nbins_tot;
  //Ndims
  map < size_t, vector < size_t > > mapOfBins;
  vector < size_t > Nbins;
  vector < double > xmins;
  vector < double > xmaxs;
  vector < double > dxs;

  //NdimxNdimxNdim... 
  vector < float > BinContent;
  vector < float > BinError;

  //Constructors
  myH();
  myH(vector < size_t > Nbins, vector < double > xmins, vector < double > xmaxs);
  myH(string fname);
  myH(TString fname);
  
  //Methods
  size_t find(vector < double > xs, bool create_mapOfBins);
  void fill(vector < double > xs);
  void fill(vector < double > xs, float w);
  float getBC(size_t i_X);
  void setBC(size_t i_X, float bc);
  float getBE(size_t i_X);
  void setBE(size_t i_X, float be);
  void getasymmetry(myH *h, bool calculateError = 1);
  void add(myH *h, float factor = 1, bool calculateError = 1);
  void add_constant(float constant);
  void divide(myH *h, bool calculateError = 1);
  void multiply(myH *h, bool calculateError = 1);
  double integral(bool calculateError = 1);
  double integral(double& Ierr);
  void scale(float factor);
  void norm();
  void dividenorm(myH *h,bool calculateError = 1);
  double getChi2(myH *h);
  void positive();
  void zero();
  void zeroErrors();
  void leveling(float BCmax,float BCmax_value);
  TH3D convert2TH3D();
  TH2D convert2TH2D();
  TH1D convert2TH1D();
  TH1D convert2TH1D(size_t i_dim);
  TH1D getBCdistribution(vector < size_t> EmptyBins);
  vector < vector < size_t > > smoothAlgorithm(float BCmin, vector < size_t> EmptyBins); 
  void smoothApply(vector < vector < size_t > > multiple_bins);
  vector < size_t > commonEmptyBins(myH *h);
  void writeToFile(string fname);
  void readFromFile(string fname);
};


