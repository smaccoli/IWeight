//#include "kde.cxx"
#include <iostream>
#include"TChain.h"
#include"TTree.h"
#include"TH2D.h"
#include"TFile.h"
using namespace std;

#include <cstdlib>
#include <string>
#include "TLeaf.h"
#include "TCanvas.h"
#include "TLegend.h"
#include "TF1.h"
#include "TH2D.h"
#include "TMath.h"
//#include "TH1D.h"
//#include "TString.h"
#include "myH.cxx"
#include "dcastyle.C"
#include "Tools.h"
#include <TObjString.h>

//#include <thread>
//#include <mutex>

static int decayID_count = 0;

TString target_name = "D2KSpi";
TString toweight_name = "D2Kpipi";


class IDecayT {
public:

  TTree *tree;
  string name;
  string title;
  size_t Nevts;
  int Nevts_start;

  //Nvars
  vector < string > weights; //values filled in constructor
  vector < double > w;

  size_t decayID;

  //Idvariables
  vector < bool > idvar;

  //Constructors
  /*
    IDecayT() {
    name="decay_"+to_string(decayID_count);
    title=name;
    decayID_count++;
    }
  */
  /*
    IDecayT(TTree *tree, string name, vector < string > weights) : tree(tree), name(name), weights(weights) {
    title = name;
    decayID_count++;
    }
  */
  IDecayT(TTree *tree, string name, string title, vector < string > weights, size_t Nevts, size_t Nevts_start) : tree(tree), name(name), title(title), weights(weights), Nevts(Nevts), Nevts_start(Nevts_start) {
    //cout << tree->GetDirectory()->GetName() << endl;
    cout << name << ": " << ((double)Nevts)/1e6 << " M events starting from entry n. " << Nevts_start << endl;
    cout << "with weights: ";
    for(size_t i_var = 0; i_var < weights.size(); i_var++) {
      cout << weights.at(i_var) << '\t';
    }
    cout << endl;
    //filling weights
    TLeaf* leaf;
    double tmp_w;
    for (size_t i_evt = Nevts_start; i_evt < Nevts_start+Nevts; i_evt++) {
      tmp_w = 1;
      for(size_t i_var = 0; i_var < weights.size(); i_var++) {
	leaf = (TLeaf*) tree->GetLeaf((const char*)weights.at(i_var).c_str());
	leaf->GetBranch()->GetEntry(i_evt);
	tmp_w *= leaf->GetValue();
      }
      w.push_back(tmp_w);
    }
    
    decayID_count++;
    
  }
  /*
    IDecayT(TTree *tree, string name, string title,  vector < string > weights) : tree(tree), name(name), title(title), weights(weights) {
    cout << title << endl;
    decayID_count++;
    }
  */
  void setAsymVariable(string asymvariable);

};

void IDecayT::setAsymVariable(string asymvariable) {

  idvar.clear();
  TLeaf* leaf;
  for (size_t i_evt = Nevts_start; i_evt < Nevts_start+Nevts; i_evt++) {
    leaf = (TLeaf*) tree->GetLeaf((const char*)asymvariable.c_str());
    leaf->GetBranch()->GetEntry(i_evt);
    idvar.push_back(leaf->GetValue() > 0 ? 1 : 0);
  }
  
}

class IDecay: public IDecayT {
public:

  //Ntargets
  // vector <IDecayT*> target_decays;
  vector <IDecay*> target_decays;
  vector <float> BCmin;
  vector <myH> target_Hs;
  vector <myH> target_Hs_start;
  vector <myH> target_Hs_smoothed;
  vector <myH> weights_Hs;
  vector <myH> toweight_Hs;
  vector <myH> toweight_Hs_start;
  vector <myH> target_Hs_asym;
  vector <myH> toweight_Hs_asym;
 
  //Ntargets
  vector < vector < vector < vector < size_t >  > > > multiple_bins;
  vector < vector < vector < vector < size_t >  > > > target_multiple_bins;

  //NtargetsxNevts
  vector < vector < string > > Iweights;
  vector < vector < double > > Iw;

  //NtargetsxNvars
  vector < vector < string > > variables;
  vector < vector < double > > s; //h for KDE
  vector < vector < double > > target_s;
  vector < vector < double > > xmin;
  vector < vector < double > > xmax;
  vector < vector < size_t > > Nbins; //{NbinsX,NbinsY}

  //Ntargetsx(NevtsxNvars)
  vector < vector < double > > vars;
  vector < vector < double > > target_vars;

  //Ntargetsx(NvarsxNbins++)
  vector < vector < double > > h_Iw;


  //loops
  int Nloops;

  

  //customizable variables
  double chi2_Ndof_max = 0.1;
  bool use_Iw_max = 1;
  double Iw_max = 20;
  double Iw_max_value = 0;
  string outFileName = "";
  TString plotDir = "";
  bool ApplySmoothing = 0;
  bool calculateResidualAsymmetry=0;
  bool applyFiducialCutsToTarget = 0;
  

  //Constructors
  IDecay(TTree *tree, string name, string title, vector < string > weights, size_t Nevts, size_t Nevts_start) : 
    IDecayT(tree,name,title,weights,Nevts,Nevts_start) 
  {
    Nloops = 0;
  }

  //Methods
  // void addTargetDistribution(IDecayT* target_decay, vector < string > variables, float BCmin);
  void addTargetDistribution(IDecay* target_decay, vector < string > variables, float BCmin);
  void KDEWeight(size_t i_target);
  void Syntetize();
  void createmyH2D(size_t i_target, bool isTarget, size_t NbinsX, size_t NbinsY);
  TH2D myH2D(size_t i_target, bool isTarget, bool isOpt);
  void Weight2D(size_t i_target);
  TH1D myH1D(size_t i_target, bool isTarget, size_t NbinsX);
  void Weight1D(size_t i_target);
  void IWeight(int Nloops_max = 10);
  // vector < myH > fillmyHs(bool isTarget);
  myH fillmyH(size_t i_target, bool isTarget, bool isPositive = 0/*if idvar is not set it is unused*/);
  myH crazyfillmyH(size_t i_target, bool isTarget);
  TCanvas* compare1D(TH1D* h_target,TH1D* h_toweight, TH1D* h_toweight_start, TString variable_name);
  TCanvas* compare1D(TH1D* h_target,TH1D* h_toweight, TH1D* h_toweight_start, TH1D* h_target_start, TString variable_name);
  TCanvas* drawResidualAsymmetry1D(TH1D* h_asym,TH1D* h_target_minus_toweight, TString variable_name);
  void weightmyHs();
  double weightmyH(size_t i_target);
  void showVariables();
  void setIWeight(string iweight);
  void fillTargets();
  void getStats(); 
};

//.cxx

// void IDecay::addTargetDistribution(IDecayT* target_decay, vector < string > variables,float BCmin) {
void IDecay::addTargetDistribution(IDecay* target_decay, vector < string > variables,float BCmin) {

 
  TLeaf* leaf;
  size_t tmp_Nvars = variables.size();
  vector < double > tmp_var[tmp_Nvars];
  vector < double > tmp_vars;
  vector < double > tmp_s;
  
  TObjArray* variables_token;
  vector < size_t > tmp_Nbins;
  vector < double > tmp_xmin;
  vector < double > tmp_xmax;
 
  for (size_t i_var = 0; i_var < tmp_Nvars; i_var++) {
    variables_token = ((TString)variables.at(i_var)).Tokenize("(,)+");
    tmp_Nbins.push_back((size_t)atoi(((TObjString *)variables_token->At(1))->String()));
    // tmp_xmin.push_back(stod(((TObjString *)variables_token->At(2))->String(),0));
    // tmp_xmax.push_back(stod(((TObjString *)variables_token->At(3))->String(),0));
    tmp_xmin.push_back(stod((const char*) ((TObjString *)variables_token->At(2))->String(),0));
    tmp_xmax.push_back(stod((const char*) ((TObjString *)variables_token->At(3))->String(),0));
    variables[i_var] = (/*(string)*/((TObjString *)variables_token->At(0))->String());
  }

  for(size_t i_var = 0; i_var < tmp_Nvars; i_var++) {
    cout << variables.at(i_var) << '\t';
    cout << tmp_Nbins.at(i_var) << '\t';
    cout << tmp_xmin.at(i_var) << '\t';
    cout << tmp_xmax.at(i_var) << endl;
  }
 
  //filling toweight_decay "histogram"
  for (size_t i_evt = Nevts_start; i_evt < Nevts_start+Nevts; i_evt++) {
    for(size_t i_var = 0; i_var < tmp_Nvars; i_var++) {
      leaf = (TLeaf*) tree->GetLeaf((const char*)variables.at(i_var).c_str());
      leaf->GetBranch()->GetEntry(i_evt);
      //cout << leaf->GetValue() << endl;
      tmp_var[i_var].push_back(leaf->GetValue());
      tmp_vars.push_back(leaf->GetValue());
    }
  }

  vars.push_back(tmp_vars);
  tmp_vars.clear();
  s.push_back(tmp_s);
  tmp_s.clear();



  // ...............................................

  //filling target_decay "histogram"
  for (size_t i_evt = target_decay->Nevts_start; i_evt < target_decay->Nevts_start+target_decay->Nevts; i_evt++) {
    for(size_t i_var = 0; i_var < tmp_Nvars; i_var++) {
      leaf = (TLeaf*) target_decay->tree->GetLeaf((const char*)variables.at(i_var).c_str());
      leaf->GetBranch()->GetEntry(i_evt);
      //cout << leaf->GetValue() << endl;
      tmp_var[i_var].push_back(leaf->GetValue());
      tmp_vars.push_back(leaf->GetValue());
    }
  }


  target_vars.push_back(tmp_vars);
  tmp_vars.clear();
  target_s.push_back(tmp_s);
  tmp_s.clear();

  target_decays.push_back(target_decay);
  this->variables.push_back(variables);


  Nbins.push_back(tmp_Nbins);
  tmp_Nbins.clear();
  xmax.push_back(tmp_xmax);
  tmp_xmax.clear();
  xmin.push_back(tmp_xmin);
  tmp_xmin.clear();

  this->BCmin.push_back(BCmin);
 
}

void IDecay::createmyH2D(size_t i_target, bool isTarget = 0, size_t NbinsX = 100, size_t NbinsY = 80) {

  float BCmin = this->BCmin.at(i_target);
  size_t Nvars = variables.at(i_target).size();
  double values[Nvars];

  this->Nbins.push_back({NbinsX,NbinsY});

  vector < vector < double > > vars;
  size_t Nevts;

  if (isTarget) {
    vars = this->target_vars;
    Nevts = target_decays.at(i_target)->Nevts;
  }
  else {
    vars = this->vars;
    Nevts = this->Nevts;
  }

  double xmin = this->xmin.at(i_target).at(0);
  double xmax = this->xmax.at(i_target).at(0);
  double ymin = this->xmin.at(i_target).at(1);
  double ymax = this->xmax.at(i_target).at(1);
  //cout << xmin << '\t' << xmax << endl;
  //cout << ymin << '\t' << ymax << endl;


  TH2D h_toweight("h_toweight","h_toweight",NbinsX,xmin,xmax,NbinsY,ymin,ymax);
  TH2D h_toweight_opt;
  TH2D h_tmp;

  size_t Ntargets = Iw.size(); // ==2
  double tmp_Iw = 1;
  for (size_t i_evt = 0; i_evt < Nevts; i_evt++) {
    for (size_t i_var = 0; i_var < Nvars; i_var++) {
      values[i_var] = vars.at(i_target).at(i_evt*Nvars+i_var);
    }
    if (!isTarget) {
      if (Ntargets > 0) {
	tmp_Iw = 1;
	for (size_t i_tar = 0; i_tar < Ntargets; i_tar++) {
	  tmp_Iw *= Iw.at(i_tar).at(i_evt);
	}
      }
    }

    h_toweight.Fill(values[0],values[1],tmp_Iw);
  }
  /*
    h_toweight.Scale(1./h_toweight.Integral());
  */

  h_toweight_opt = TH2D(h_toweight);
  h_toweight_opt.SetName("h_toweight_opt");
  h_toweight_opt.SetTitle("h_toweight_opt");
  h_tmp = TH2D(h_toweight);
  h_tmp.SetName("h_tmp");
  h_tmp.SetTitle("h_tmp");


  double bcmin = Nevts;
  double bcmin_old = 0;
  size_t bcmin_i_X, bcmin_i_Y;
  double bc;
  double close_bcmin = Nevts;
  size_t close_bcmin_i_X, close_bcmin_i_Y;
  double close_bc;

  vector < vector < vector < size_t >  > > multiple_bins;
  vector < size_t > bcmin_i;
  vector < size_t > close_bcmin_i;;
  size_t Nbins, Nbins_A, Nbins_B;
  bool done;

  vector < pair < double, pair <size_t, size_t> > >  lowbc_bins;
  bc = 0;
  for(size_t i_X = 1; i_X <= NbinsX; i_X++) {
    for(size_t i_Y = 1; i_Y <= NbinsY; i_Y++) {
      bc = h_tmp.GetBinContent(i_X,i_Y);
      if (bc <= BCmin)
	lowbc_bins.push_back(make_pair(bc,make_pair(i_X,i_Y)));
    }
  }

  sort(lowbc_bins.begin(), lowbc_bins.end());

  size_t i = 0;
  size_t lowbc_bins_size = lowbc_bins.size();
  while(i < lowbc_bins_size) {
    bc = 0;
    done = 0;
    bcmin_old = bcmin;
    bcmin = Nevts+1;
    close_bcmin = Nevts+1;

    //find get bin with lowest yields: (bcmin)
    bcmin = lowbc_bins.at(i).first;
    bcmin_i_X = lowbc_bins.at(i).second.first;
    bcmin_i_Y = lowbc_bins.at(i).second.second;
    i++;

    // cout << bcmin_i_X << '\t' << bcmin_i_Y << '\t' << bcmin << endl;

    //find close bin with lowest yield
    for(size_t i_X = bcmin_i_X - 1; i_X <= bcmin_i_X + 1; i_X++) {
      if (close_bcmin == bcmin)
	break;
      for(size_t i_Y = bcmin_i_Y -1; i_Y <= bcmin_i_Y + 1; i_Y++) {
	if (close_bcmin == bcmin)
	  break;

	if (i_X < 1 || i_Y < 1 || i_X > NbinsX || i_Y > NbinsY || (i_X == bcmin_i_X && i_Y == bcmin_i_Y))
	  continue;
	//no diagonal bins
	/*
	  if (!(i_X == bcmin_i_X || i_Y == bcmin_i_Y))
	  continue;
	*/
	close_bc = h_tmp.GetBinContent(i_X,i_Y);
	//cout <<"bc = " << i_X << '\t' << i_Y << '\t' << close_bc << endl;
	if( close_bc <= close_bcmin ) {
	  close_bcmin = close_bc;
	  close_bcmin_i_X = i_X;
	  close_bcmin_i_Y = i_Y;
	  //cout << "here" << endl;
	}
      }
    }

    //cout << close_bcmin_i_X << '\t' << close_bcmin_i_Y << '\t' << close_bcmin << endl;

    h_tmp.SetBinContent(bcmin_i_X,bcmin_i_Y,Nevts);
    h_tmp.SetBinContent(close_bcmin_i_X,close_bcmin_i_Y,(bcmin+close_bcmin)/2);

    bcmin_i = {bcmin_i_X,bcmin_i_Y};
    close_bcmin_i = {close_bcmin_i_X,close_bcmin_i_Y};

    //merge of two multibins
    if(!done) {
      for (size_t i_A = 0; i_A < multiple_bins.size(); i_A++) {
	Nbins_A = multiple_bins.at(i_A).size();
	for (size_t j_A = 0; j_A < Nbins_A; j_A++) {
	  if (multiple_bins.at(i_A).at(j_A) == bcmin_i) {
	    for (size_t i_B = 0; i_B < multiple_bins.size(); i_B++) {
	      Nbins_B = multiple_bins.at(i_B).size();
	      for (size_t j_B = 0; j_B < Nbins_B; j_B++) {
		if (multiple_bins.at(i_B).at(j_B) == close_bcmin_i) {
		  if (i_A != i_B) {
		    for (size_t k_B = 0; k_B < Nbins_B; k_B++) {
		      multiple_bins.at(i_A).push_back(multiple_bins.at(i_B).at(k_B));
		    }
		    multiple_bins.at(i_B).clear();
		    Nbins_B = 0;
		  }
		  else {

		  }
		  done = 1;
		}
	      }
	    }
	  }
	}
      }
    }

    //one of the two bins altready in a multibin
    if(!done) {
      for (size_t i = 0; i < multiple_bins.size(); i++) {
	Nbins = multiple_bins.at(i).size();
	for (size_t j = 0; j < Nbins; j++) {
	  if (multiple_bins.at(i).at(j) == bcmin_i) {
	    multiple_bins.at(i).push_back(close_bcmin_i);
	    done = 1;
	  }
	  else if (multiple_bins.at(i).at(j) == close_bcmin_i) {
	    multiple_bins.at(i).push_back(bcmin_i);
	    done = 1;
	    break;
	  }
	}
      }
    }

    if (!done)
      multiple_bins.push_back({{bcmin_i_X,bcmin_i_Y},{close_bcmin_i_X,close_bcmin_i_Y}});

  }

  if (Nloops == 0) {
    if (isTarget)
      this->target_multiple_bins.push_back(multiple_bins);
    else
      this->multiple_bins.push_back(multiple_bins);
  }
  else {
    if (!isTarget) {
      this->multiple_bins.at(i_target).clear();
      this->multiple_bins[i_target] = multiple_bins;
    }
  }

  cout << "Binning optimized with BinContent > " << BCmin;
  cout << ". Nmultiplebins = " << multiple_bins.size();
  cout << endl;

}

TH2D IDecay::myH2D(size_t i_target, bool isTarget = 0, bool isOpt = 1) {

  size_t Nvars = 2;//variables.at(i_target).size(); // ==2
  size_t Ntargets = Iw.size(); // ==1

  double xmin = this->xmin.at(i_target).at(0);
  double xmax = this->xmax.at(i_target).at(0);
  double ymin = this->xmin.at(i_target).at(1);
  double ymax = this->xmax.at(i_target).at(1);
  size_t NbinsX = this->Nbins.at(i_target).at(0);
  size_t NbinsY = this->Nbins.at(i_target).at(1);
  size_t Nbins;

  vector < vector < vector < size_t >  > > multiple_bins;
  if (isTarget)
    multiple_bins = this->target_multiple_bins.at(i_target);
  else
    multiple_bins = this->multiple_bins.at(i_target);

  vector < vector < double > > vars;
  size_t Nevts;

  if (isTarget) {
    vars = this->target_vars;
    Nevts = target_decays.at(i_target)->Nevts;
  }
  else {
    vars = this->vars;
    Nevts = this->Nevts;
  }

  TH2D h("h","h",NbinsX,xmin,xmax,NbinsY,ymin,ymax);
  TH2D h_opt;

  double values[Nvars];
  double tmp_Iw = 1;
  for (size_t i_evt = 0; i_evt < Nevts; i_evt++) {
    for (size_t i_var = 0; i_var < Nvars; i_var++) {
      values[i_var] = vars.at(i_target).at(i_evt*Nvars+i_var);
    }
    if (!isTarget) {
      if (Ntargets > 0) {
	tmp_Iw = 1;
	for (size_t i_tar = 0; i_tar < Ntargets; i_tar++) {
	  tmp_Iw *= Iw.at(i_tar).at(i_evt);
	}
      }
    }

    h.Fill(values[0],values[1],tmp_Iw);
  }

  h_opt = TH2D(h);
  h_opt.SetName("h_opt");
  h_opt.SetTitle("h_opt");


  double bc;
  size_t Nmultiplebins = multiple_bins.size();
  for (size_t i = 0; i < Nmultiplebins; i++) {
    bc = 0;
    Nbins = multiple_bins.at(i).size();
    for (size_t j = 0; j < Nbins; j++) {//Nbins per set
      bc += h_opt.GetBinContent(multiple_bins.at(i).at(j).at(0),multiple_bins.at(i).at(j).at(1));

    }
    for (size_t j = 0; j < Nbins; j++) {//Nbins per set
      h_opt.SetBinContent(multiple_bins.at(i).at(j).at(0),multiple_bins.at(i).at(j).at(1), bc/Nbins);
      //h_forget->SetBinContent(multiple_bins.at(i).at(j).at(0),multiple_bins.at(i).at(j).at(1), bc/Nbins);
    }
  }

  return isOpt ? h_opt : h;
}

/*
  void IDecay::Weight2D(size_t i_target) {

  createmyH2D(i_target);
  if (Nloops == 0) {
  createmyH2D(i_target,1);
  }

  size_t Nvars = 2;//variables.at(i_target).size(); // ==2

  TFile *f = TFile::Open(Form("after_%d_%d.root",(int)i_target,(int)Nloops),"RECREATE");

  TH2D h_opt_toweight = (TH2D) myH2D(i_target);
  h_opt_toweight.SetName("h_opt_toweight");
  h_opt_toweight.SetTitle("h_opt_toweight");
  TH2D h_opt_target = (TH2D) myH2D(i_target,1);
  h_opt_target.SetName("h_opt_target");
  h_opt_target.SetTitle("h_opt_target");
  TH2D h_toweight = (TH2D) myH2D(i_target,0,0);
  h_toweight.SetName("h_toweight");
  h_toweight.SetTitle("h_toweight");
  TH2D h_target = (TH2D) myH2D(i_target,1,0);
  h_target.SetName("h_target");
  h_target.SetTitle("h_target");
  TH2D h_opt_weights = TH2D(h_opt_target);
  h_opt_weights.SetName("h_opt_weights");
  h_opt_weights.SetTitle("h_opt_weights");
  h_opt_weights.Divide(&h_opt_toweight);
  h_opt_weights.Scale(h_opt_toweight.Integral()/h_opt_target.Integral());

  vector < double > tmp_Iw;

  double factor;
  double values[Nvars];
  for (size_t i_evt = 0; i_evt < Nevts; i_evt++) {
  for (size_t i_var = 0; i_var < Nvars; i_var++) {
  values[i_var] = vars.at(i_target).at(i_evt*Nvars+i_var);
  }
  factor = h_opt_weights.GetBinContent(h_opt_weights.FindBin(values[0],values[1]));
  if (Nloops != 0)
  factor *= Iw.at(i_target).at(i_evt);
  tmp_Iw.push_back(factor);

  if (i_evt%int(Nevts/100*20) == 0) {
  if(Nloops != 0)
  cout << "IWeight_" << i_target <<  "  old: "<< Iw.at(i_target).at(i_evt) << "  new: " << factor << "  factor: " << factor/Iw.at(i_target).at(i_evt)<< endl;
  else
  cout << "IWeight_" << i_target <<  "  old: "<< 1 << "  new: " << factor << endl;
  }
  }

  if (Nloops == 0)
  Iw.push_back(tmp_Iw);
  else {
  Iw.at(i_target).clear();
  Iw[i_target] = tmp_Iw;
  }
  tmp_Iw.clear();

  TH2D h_toweight_rew = (TH2D) myH2D(i_target,0,0);
  h_toweight_rew.SetName("h_toweight_rew");
  h_toweight_rew.SetTitle("h_toweight_rew");


  double chi2 = 0;
  double scale_factor = h_toweight_rew.Integral()/h_target.Integral();
  for(size_t i_X = 1; i_X <= h_toweight_rew.GetNbinsX(); i_X++) {
  for(size_t i_Y = 1; i_Y <= h_toweight_rew.GetNbinsY(); i_Y++) {
  if ((pow(h_toweight_rew.GetBinError(i_X,i_Y),2)+pow(h_target.GetBinError(i_X,i_Y),2)) != 0)
  chi2 += pow(h_toweight_rew.GetBinContent(i_X,i_Y) - scale_factor*h_target.GetBinContent(i_X,i_Y),2)/(pow(h_toweight_rew.GetBinError(i_X,i_Y),2)+pow(scale_factor*h_target.GetBinError(i_X,i_Y),2));
  }
  }

  cout << "chi2 = " << chi2 <<  endl;

  f->Write();
  f->Close();

  }
*/
TH1D IDecay::myH1D(size_t i_target, bool isTarget = 0, size_t NbinsX = 100) {

  size_t Nvars = 1;//variables.at(i_target).size(); // ==1
  size_t Ntargets = Iw.size(); // ==1


  double xmin = this->xmin.at(i_target).at(0);
  double xmax = this->xmax.at(i_target).at(0);
  //tomodify in AddTarget...
  //size_t NbinsX = this->Nbins.at(i_target).at(0);

  vector < vector < double > > vars;
  size_t Nevts;

  if (isTarget) {
    vars = this->target_vars;
    Nevts = target_decays.at(i_target)->Nevts;
  }
  else {
    vars = this->vars;
    Nevts = this->Nevts;
  }


  TH1D h("h","h",NbinsX,xmin,xmax);

  double values[Nvars];
  double tmp_Iw = 1;
  for (size_t i_evt = 0; i_evt < Nevts; i_evt++) {
    for (size_t i_var = 0; i_var < Nvars; i_var++) {
      values[i_var] = vars.at(i_target).at(i_evt*Nvars+i_var);
    }
    if (!isTarget) {
      if (Ntargets > 0) {
	tmp_Iw = 1;
	for (size_t i_tar = 0; i_tar < Ntargets; i_tar++) {
	  tmp_Iw *= Iw.at(i_tar).at(i_evt);
	}
      }
    }
    h.Fill(values[0],tmp_Iw);
  }

  return h;
}
/*
  void IDecay::Weight1D(size_t i_target) {

  size_t Nvars = 1;//variables.at(i_target).size(); // ==1

  TFile *f = TFile::Open(Form("after_%d_%d.root",(int)i_target,(int)Nloops),"RECREATE");

  TH1D h_toweight = (TH1D) myH1D(i_target);
  h_toweight.SetName("h_toweight");
  h_toweight.SetTitle("h_toweight");
  TH1D h_target = (TH1D) myH1D(i_target,1);
  h_target.SetName("h_target");
  h_target.SetTitle("h_target");
  TH1D h_weights = TH1D(h_target);
  h_weights.SetName("h_weights");
  h_weights.SetTitle("h_weights");
  h_weights.Divide(&h_toweight);
  h_weights.Scale(h_toweight.Integral()/h_target.Integral());

  vector < double > tmp_Iw;

  double factor;
  double values[Nvars];
  for (size_t i_evt = 0; i_evt < Nevts; i_evt++) {
  for (size_t i_var = 0; i_var < Nvars; i_var++) {
  values[i_var] = vars.at(i_target).at(i_evt*Nvars+i_var);
  }
  factor = h_weights.GetBinContent(h_weights.FindBin(values[0]));
  if (Nloops != 0)
  factor *= Iw.at(i_target).at(i_evt);
  if (factor > 100)
  factor = 1;

  tmp_Iw.push_back(factor);

  if (i_evt%int(Nevts/100*20) == 0) {
  if(Nloops != 0)
  cout << "IWeight1D_" << i_target <<  "  old: "<< factor/Iw.at(i_target).at(i_evt) << "  new: " << factor << "  factor: " << Iw.at(i_target).at(i_evt)<< endl;
  else
  cout << "IWeight1D_" << i_target <<  "  old: "<< 1 << "  new: " << factor << endl;
  }
  }

  if (Nloops == 0)
  Iw.push_back(tmp_Iw);
  else {
  Iw.at(i_target).clear();
  Iw[i_target] = tmp_Iw;
  }
  tmp_Iw.clear();

  TH1D h_toweight_rew = (TH1D) myH1D(i_target,0);
  h_toweight_rew.SetName("h_toweight_rew");
  h_toweight_rew.SetTitle("h_toweight_rew");

  double chi2 = 0;
  double scale_factor = h_toweight_rew.Integral()/h_target.Integral();
  for(size_t i_X = 1; i_X <= h_toweight_rew.GetNbinsX(); i_X++) {
  if ((pow(h_toweight_rew.GetBinError(i_X),2)+pow(h_target.GetBinError(i_X),2)) != 0)
  chi2 += pow(h_toweight_rew.GetBinContent(i_X) - scale_factor*h_target.GetBinContent(i_X),2)/(pow(h_toweight_rew.GetBinError(i_X),2)+pow(scale_factor*h_target.GetBinError(i_X),2));
  }

  cout << "chi2 = " << chi2 << endl;

  f->Write();
  f->Close();

  }
*/
void IDecay::IWeight(int Nloops_max) {

  target_Hs.clear();
  target_Hs_smoothed.clear();
  toweight_Hs_start.clear();
  for (size_t i_target = 0; i_target < target_decays.size(); i_target++) {
    target_Hs.push_back(fillmyH(i_target,1));
    target_Hs_smoothed.push_back(target_Hs.at(i_target));
    //target_Hs_smoothed.at(i_target).smoothApply(target_Hs_smoothed.at(i_target).smoothAlgorithm(BCmin.at(i_target)));
    toweight_Hs_start.push_back(fillmyH(i_target,0));
    //toweight_Hs_start.at(i_target).positive();
  }


  vector < bool > agreement;
  bool agreements;
  while(agreements == 0 && Nloops < Nloops_max) { 
    cout << ">>Loop #"<< Nloops << endl;
    agreements = 1;
    for (size_t i_target = 0; i_target < target_decays.size(); i_target++) {
      //showVariables();
      agreement.push_back(weightmyH(i_target) < chi2_Ndof_max);
      agreements = agreements && agreement.at(i_target);
    }   
    agreement.clear();
    cout << "agreements: " << agreements << endl;
    cout << "Nloops: " << Nloops << endl;
   
    Nloops++; 
  }
  
}

myH IDecay::fillmyH(size_t i_target, bool isTarget, bool isPositive) {

 
  vector < vector < double > > vars;
  size_t Nevts;
  vector < double > w;
  vector < vector < double > > Iw;
  vector < bool > idvar;
  if (!isTarget) {
    vars = this->vars;
    Nevts = this->Nevts;
    w = this->w;
    Iw = this->Iw;
    idvar = this->idvar;
  }
  else {
    vars = this->target_vars;
    Nevts = target_decays.at(i_target)->Nevts;
    w = target_decays.at(i_target)->w;
    Iw = target_decays.at(i_target)->Iw;
    idvar = target_decays.at(i_target)->idvar;
  }
  
  vector < double > values;
  size_t Nvars = variables.at(i_target).size();
  myH h(Nbins.at(i_target),xmin.at(i_target),xmax.at(i_target));

  double tmp_w = 1;
  double tmp_Iw = 1;
  if (!isTarget) {
    for (size_t i_evt = 0; i_evt < Nevts; i_evt++) {
      
      tmp_w = 1;
      if (w.size() != 0)
	tmp_w = w.at(i_evt);
   
      if (idvar.size() != 0) {
	tmp_w *= isPositive ? idvar.at(i_evt) : !(idvar.at(i_evt));
      }
      
      tmp_Iw = 1;
      for (size_t i_tar = 0; i_tar < Iw.size()/*Ntargets*/; i_tar++) {
	tmp_Iw *= Iw.at(i_tar).at(i_evt);
      }
      

      values.clear();
      for (size_t i_var = 0; i_var < Nvars; i_var++) {
	values.push_back(vars.at(i_target).at(i_evt*Nvars+i_var));
      }

      h.fill(values,tmp_Iw*tmp_w);
    }
  }
  else {

    for (size_t i_evt = 0; i_evt < Nevts; i_evt++) {
    
      tmp_w = 1;
      if (w.size() != 0)
	tmp_w = w.at(i_evt);
      
      if (idvar.size() != 0) {
	tmp_w *= isPositive ? idvar.at(i_evt) : !(idvar.at(i_evt));
      }
    
      //the target can have been previously weighted
      tmp_Iw = 1;
      for (size_t i_tar = 0; i_tar < Iw.size()/*NtargetsOfTheTarget*/; i_tar++) {
      	tmp_Iw *= Iw.at(i_tar).at(i_evt);
      }
      
      values.clear();
      for (size_t i_var = 0; i_var < Nvars; i_var++) {
	values.push_back(vars.at(i_target).at(i_evt*Nvars+i_var));
      }
      
      
      h.fill(values,tmp_Iw*tmp_w);
    }
 
  }

  return h;
}

// vector < myH > IDecay::fillmyHs(bool isTarget) {

//   vector < vector < double > > vars;
//   size_t Nevts;

//   if (!isTarget) {
//     vars = this->vars;
//     Nevts = this->Nevts;
//   }

//   size_t Ntargets = target_decays.size();
//   vector < myH > Hs;
//   vector < double > values;
//   size_t Nvars;
//   for (size_t i_target = 0; i_target < Ntargets; i_target++) {
//     myH h(Nbins.at(i_target),xmin.at(i_target),xmax.at(i_target));
//     Hs.push_back(h);
//   }
//   double tmp_Iw = 1;
//   if (!isTarget) {
//     for (size_t i_evt = 0; i_evt < Nevts; i_evt++) {
//       if (Nloops > 0) {
// 	tmp_Iw = 1;
// 	for (size_t i_target = 0; i_target < Ntargets; i_target++) {
// 	  tmp_Iw *= Iw.at(i_target).at(i_evt);
// 	}
//       }
//       for (size_t i_target = 0; i_target < Ntargets; i_target++) {
// 	Nvars = variables.at(i_target).size();
// 	for (size_t i_var = 0; i_var < Nvars; i_var++) {
// 	  values.push_back(vars.at(i_target).at(i_evt*Nvars+i_var));
// 	}
// 	Hs.at(i_target).fill(values,tmp_Iw);
// 	values.clear();
//       }
//     }
//   }
//   else {
//     for (size_t i_target = 0; i_target < Ntargets; i_target++) {
//       vars = this->target_vars;
//       Nevts = target_decays.at(i_target)->Nevts;
//       for (size_t i_evt = 0; i_evt < Nevts; i_evt++) {
// 	Nvars = variables.at(i_target).size();
// 	values.clear();
// 	for (size_t i_var = 0; i_var < Nvars; i_var++) {
// 	  values.push_back(vars.at(i_target).at(i_evt*Nvars+i_var));
// 	}
// 	Hs.at(i_target).fill(values,tmp_Iw);
//       }
//     }
//   }

//   return Hs;
// }

double IDecay::weightmyH(size_t i_target) {
 

  target_name = target_decays.at(i_target)->title;
  toweight_name = title;
  
  myH target_H = target_Hs_smoothed.at(i_target);
  myH toweight_H = fillmyH(i_target,0);
  size_t Nvars;
  Nvars = variables.at(i_target).size();
  vector < double > values;

  //TFile * f = new TFile(Form("after_%d_%d.root",Nloops,(int)i_target),"RECREATE");

  TH1D hBC_target;
  TH1D hBC_toweight;
  
  TH1D h1D_target;
  TH1D h1D_toweight;
  TH1D h1D_weights;
  TH1D h1D_weights_final;
  TH2D h2D_target;
  TH2D h2D_toweight;
  TH2D h2D_weights;
  TH2D h2D_weights_final;
  TH3D h3D_target;
  TH3D h3D_toweight;
  TH3D h3D_weights;
  TH3D h3D_weights_final;
  TH1D h1D_toweight_start;
  TH2D h2D_toweight_start;
  TH3D h3D_toweight_start;
  // TH1D h1D_target_start;
  // TH2D h2D_target_start;
  // TH3D h3D_target_start;

  bool newvariable = weights_Hs.size() < i_target+1;
   
  //agreement
  double chi2 = target_Hs.at(i_target).getChi2(&toweight_H);
  
  vector <size_t > commonEB = toweight_H.commonEmptyBins(&target_H); 
  int Ndof = toweight_H.Nbins_tot - commonEB.size();
  
  
  cout <<">Iteration #"<< i_target << endl;
  for (size_t i_var = 0; i_var < Nvars; i_var++) {
    cout << variables.at(i_target).at(i_var) << '\t';
  }
  cout << endl;
  
  double chi2_Ndof = /*TMath::Prob(chi2,Ndof);*/chi2/Ndof;
  cout << "chi2/Ndof = "<< chi2 << "/" << Ndof << " = " << chi2_Ndof << endl;
  
  hBC_target = target_H.getBCdistribution(commonEB);
  hBC_target.SetName("hBC_target");
  hBC_target.SetTitle("hBC_target");
  hBC_toweight = target_H.getBCdistribution(commonEB);
  hBC_toweight.SetName("hBC_toweight");
  hBC_toweight.SetTitle("hBC_toweight");
  
  if (Nvars == 3) {
    h3D_target = target_H.convert2TH3D();
    h3D_target.SetName("h3D_target");
    h3D_target.SetTitle("h3D_target");
    h3D_toweight = toweight_H.convert2TH3D();
    h3D_toweight.SetName("h3D_toweight");
    h3D_toweight.SetTitle("h3D_toweight");
    h3D_toweight_start = toweight_Hs_start.at(i_target).convert2TH3D();
    h3D_toweight_start.SetName("h3D_toweight_start");
    h3D_toweight_start.SetTitle("h3D_toweight_start");
    
    compare1D(h3D_target.ProjectionX(),h3D_toweight.ProjectionX(),h3D_toweight_start.ProjectionX(),variables.at(i_target).at(0));
    compare1D(h3D_target.ProjectionY(),h3D_toweight.ProjectionY(),h3D_toweight_start.ProjectionY(),variables.at(i_target).at(1));
    compare1D(h3D_target.ProjectionZ(),h3D_toweight.ProjectionZ(),h3D_toweight_start.ProjectionZ(),variables.at(i_target).at(2));

  
    //smoothing
    if (ApplySmoothing) {
      auto k = toweight_H.smoothAlgorithm(BCmin.at(i_target),commonEB);
      toweight_H.smoothApply(k);
      if(Nloops == 0 || newvariable) {
	target_H.smoothApply(target_H.smoothAlgorithm(BCmin.at(i_target),commonEB));
	target_Hs_smoothed[i_target] = target_H;
      }
    } 

    //toweight_H.scale(toweight_H.integral());
    h3D_target = target_H.convert2TH3D();
    h3D_target.SetName("h3D_target");
    h3D_target.SetTitle("h3D_target");

    h3D_toweight = toweight_H.convert2TH3D();
    h3D_toweight.SetName("h3D_toweight");
    h3D_toweight.SetTitle("h3D_toweight");
  
  }
  else  if (Nvars == 2) {
    h2D_target = target_H.convert2TH2D();
    h2D_target.SetName("h2D_target");
    h2D_target.SetTitle("h2D_target");
    h2D_toweight = toweight_H.convert2TH2D();
    h2D_toweight.SetName("h2D_toweight");
    h2D_toweight.SetTitle("h2D_toweight");
    h2D_toweight_start = toweight_Hs_start.at(i_target).convert2TH2D();
    h2D_toweight_start.SetName("h2D_toweight_start");
    h2D_toweight_start.SetTitle("h2D_toweight_start");
  
    compare1D(h2D_target.ProjectionX(),h2D_toweight.ProjectionX(),h2D_toweight_start.ProjectionX(),variables.at(i_target).at(0));
    compare1D(h2D_target.ProjectionY(),h2D_toweight.ProjectionY(),h2D_toweight_start.ProjectionY(),variables.at(i_target).at(1));
  
    //smoothing
    if (ApplySmoothing) {
      //auto k = toweight_H.smoothAlgorithm(BCmin.at(i_target),commonEB);
      //toweight_H.smoothApply(k);
      if(Nloops == 0 || newvariable) {
	target_H.smoothApply(target_H.smoothAlgorithm(BCmin.at(i_target),commonEB));
	target_Hs_smoothed[i_target] = target_H;
      }
    }

    h2D_target = target_H.convert2TH2D();
    h2D_target.SetName("h2D_target");
    h2D_target.SetTitle("h2D_target");

    h2D_toweight = toweight_H.convert2TH2D();
    h2D_toweight.SetName("h2D_toweight");
    h2D_toweight.SetTitle("h2D_toweight");
    
  }
  else if (Nvars == 1) {
    h1D_target = target_H.convert2TH1D();
    h1D_target.SetName("h1D_target");
    h1D_target.SetTitle("h1D_target");
    h1D_toweight = toweight_H.convert2TH1D();
    h1D_toweight.SetName("h1D_toweight");
    h1D_toweight.SetTitle("h1D_toweight");
    h1D_toweight_start = toweight_Hs_start.at(i_target).convert2TH1D();
    h1D_toweight_start.SetName("h1D_toweight_start");
    h1D_toweight_start.SetTitle("h1D_toweight_start");
    
    compare1D(&h1D_target,&h1D_toweight,&h1D_toweight_start,variables.at(i_target).at(0));

  }
  else if (Nvars <= 6) {

    for (size_t i_dim = 0; i_dim < Nvars; i_dim++) {
    
      h1D_target = target_H.convert2TH1D(i_dim);
      h1D_target.SetName((const char*)("h1D_"+(TString)variables.at(i_target).at(i_dim)+"_target"));
      h1D_target.SetTitle((const char*)("h1D_"+(TString)variables.at(i_target).at(i_dim)+"_target"));
   
      h1D_toweight = toweight_H.convert2TH1D(i_dim);
      h1D_toweight.SetName((const char*)("h1D_"+(TString)variables.at(i_target).at(i_dim)+"_toweight"));
      h1D_toweight.SetTitle((const char*)("h1D_"+(TString)variables.at(i_target).at(i_dim)+"_toweight"));
      h1D_toweight_start = toweight_Hs_start.at(i_target).convert2TH1D(i_dim);
      h1D_toweight_start.SetName((const char*)("h1D_"+(TString)variables.at(i_target).at(i_dim)+"_toweight_start"));
      h1D_toweight_start.SetTitle((const char*)("h1D_"+(TString)variables.at(i_target).at(i_dim)+"_toweight_start"));
      
      compare1D(&h1D_target,&h1D_toweight,&h1D_toweight_start,variables.at(i_target).at(i_dim));
    }

    if (ApplySmoothing) {
      toweight_H.smoothApply(toweight_H.smoothAlgorithm(BCmin.at(i_target),commonEB));
      if(Nloops == 0 || newvariable) {
	target_H.smoothApply(target_H.smoothAlgorithm(BCmin.at(i_target),commonEB));
	target_Hs_smoothed[i_target] = target_H;
      }
    }

  }


  target_H.positive();
  toweight_H.positive();
  target_H.dividenorm(&toweight_H,/*bool calculateError = */1);
   
  
  if (Nloops == 0 || newvariable )
    weights_Hs.push_back(target_H);
  else
    weights_Hs.at(i_target).multiply(&target_H,/*bool calculateError = */1);

  if (use_Iw_max)
    weights_Hs.at(i_target).leveling(Iw_max,Iw_max_value);

  weights_Hs.at(i_target).writeToFile(outFileName+"_"+to_string(i_target)+".myH");


  if (Nvars == 3) {
  
    h3D_weights = target_H.convert2TH3D();
    h3D_weights.SetName("h3D_weights");
    h3D_weights.SetTitle("h3D_weights");

    h3D_weights_final = weights_Hs.at(i_target).convert2TH3D();
    h3D_weights_final.SetName("h3D_weights_final");
    h3D_weights_final.SetTitle("h3D_weights_final");
  }
  else if (Nvars == 2) {
  
    h2D_weights = target_H.convert2TH2D();
    h2D_weights.SetName("h2D_weights");
    h2D_weights.SetTitle("h2D_weights");

    h2D_weights_final = weights_Hs.at(i_target).convert2TH2D();
    h2D_weights_final.SetName("h2D_weights_final");
    h2D_weights_final.SetTitle("h2D_weights_final");
  }
  else if (Nvars == 1) {
  
    h1D_weights = target_H.convert2TH1D();
    h1D_weights.SetName("h1D_weights");
    h1D_weights.SetTitle("h1D_weights");

    h1D_weights_final = weights_Hs.at(i_target).convert2TH1D();
    h1D_weights_final.SetName("h1D_weights_final");
    h1D_weights_final.SetTitle("h1D_weights_final");
  }
  
  //TTree * t = new TTree("ntp","ntp");
  //double weight_ND;
  //t->Branch("weight_ND",&weight_ND);
  
  
  vector < double > tmp_Iw;

  for (size_t i_evt = 0; i_evt < Nevts; i_evt++) {
    values.clear();
    for (size_t i_var = 0; i_var < Nvars; i_var++) {
      values.push_back(vars.at(i_target).at(i_evt*Nvars+i_var));
    }

    tmp_Iw.push_back(weights_Hs.at(i_target).getBC(weights_Hs.at(i_target).find(values)));
    
    if (i_evt%int(Nevts/100*20) == 0) {
      if (!(Nloops == 0 || newvariable))
	cout << "IWeight_" << i_target <<  "  old: "<< Iw.at(i_target).at(i_evt) << "  new: " << tmp_Iw.at(i_evt) << "  factor: " << (Iw.at(i_target).at(i_evt) != 0 ? tmp_Iw.at(i_evt)/Iw.at(i_target).at(i_evt) : 0) << endl;
      else
	cout << "IWeight_" << i_target <<  "  old: "<< 1 << "  new: " << tmp_Iw.at(i_evt) << "  factor: " << tmp_Iw.at(i_evt) << endl;
    }

    //weight_ND = tmp_Iw.at(i_evt);
    
    //t->Fill();
  }

  if (Nloops == 0 || newvariable)
    Iw.push_back(tmp_Iw);
  else
    Iw[i_target] = tmp_Iw;


  //f->Write();
  //f->Close();
 

  if(applyFiducialCutsToTarget) {
  
    //modify weight "w"  of the target sample:
    bool target_has_w = target_decays.at(i_target)->w.size() != 0;
    double tmp_Iw;
    for (size_t i_evt = 0; i_evt < target_decays.at(i_target)->Nevts; i_evt++) {
      values.clear();
      for (size_t i_var = 0; i_var < Nvars; i_var++) {
	values.push_back(target_vars.at(i_target).at(i_evt*Nvars+i_var));
      }
      tmp_Iw = weights_Hs.at(i_target).getBC(weights_Hs.at(i_target).find(values));
      
      if (target_has_w)
   	target_decays.at(i_target)->w.at(i_evt) *= tmp_Iw > 0;
      else
   	target_decays.at(i_target)->w.push_back( tmp_Iw > 0);
    }
 
  }



  return chi2_Ndof;
}

/*
  void IDecay::weightmyHs() {

  auto target_Hs = fillmyHs(1);
  auto toweight_Hs = fillmyHs(0);
  size_t Ntargets = toweight_Hs.size();
  size_t Nvars;
  vector < double > values;

  TFile * f = new TFile(Form("after_%d.root",Nloops),"RECREATE");

  TH2D h_target[Ntargets];
  TH2D h_toweight[Ntargets];
  for (size_t i_target = 0; i_target < Ntargets; i_target++) {
  //smooth
  //target_Hs.at(i_target).smoothApply(target_Hs.at(i_target).smoothAlgorithm(10));
  //toweight_Hs.at(i_target).smoothApply(toweight_Hs.at(i_target).smoothAlgorithm(10));
  //calculate chi2 and cout

  h_target[i_target] = target_Hs.at(i_target).convert2TH2D();
  h_target[i_target].SetName(Form("h_target_%d",(int)i_target));
  h_target[i_target].SetTitle(Form("h_target_%d",(int)i_target));
  h_toweight[i_target] = toweight_Hs.at(i_target).convert2TH2D();
  h_toweight[i_target].SetName(Form("h_toweight_%d",(int)i_target));
  h_toweight[i_target].SetTitle(Form("h_toweight_%d",(int)i_target));

  target_Hs.at(i_target).dividenorm(&toweight_Hs.at(i_target));

  }

  f->Write();
  f->Close();

  vector < double > tmp_Iw;
  vector < vector < double > > Iw;


  for (size_t i_target = 0; i_target < Ntargets; i_target++) {
  Nvars = variables.at(i_target).size();
  for (size_t i_evt = 0; i_evt < Nevts; i_evt++) {
  values.clear();
  for (size_t i_var = 0; i_var < Nvars; i_var++) {
  values.push_back(vars.at(i_target).at(i_evt*Nvars+i_var));
  }
  if (Nloops != 0)
  tmp_Iw.push_back(this->Iw.at(i_target).at(i_evt)*target_Hs.at(i_target).getBC(target_Hs.at(i_target).find(values)));
  else
  tmp_Iw.push_back(target_Hs.at(i_target).getBC(target_Hs.at(i_target).find(values)));

  if (i_evt%int(Nevts/100*20) == 0) {
  if (Nloops != 0)
  cout << "IWeight2D_" << i_target <<  "  old: "<< this->Iw.at(i_target).at(i_evt) << "  new: " << tmp_Iw.at(i_evt) << "  factor: " << tmp_Iw.at(i_evt)/this->Iw.at(i_target).at(i_evt) << endl;
  else
  cout << "IWeight2D_" << i_target <<  "  old: "<< 1 << "  new: " << tmp_Iw.at(i_evt) << "  factor: " << tmp_Iw.at(i_evt) << endl;
  }


  }
  Iw.push_back(tmp_Iw);


  }

  this->Iw = Iw;

  Nloops++;
  }
*/

TCanvas* IDecay::compare1D(TH1D* h_target,TH1D* h_toweight,TH1D* h_toweight_start,TString variable_name) {
  TCanvas* Canvas = new TCanvas(variable_name,variable_name, 50,50, 1000,700);
  
  Canvas->cd();

  h_target->Scale(1./h_target->Integral());
  h_target->SetMarkerColor(kRed);
  h_target->SetLineColor(kRed);
  h_target->SetXTitle(findAxisTitle(variable_name));
  h_target->SetMarkerSize(1.5);
  h_target->SetMarkerStyle(25);
  h_target->SetYTitle("Candidates (arbitrary scale)");

  h_toweight->Scale(1./h_toweight->Integral());
  h_toweight->SetMarkerColor(kBlue);
  h_toweight->SetLineColor(kBlue);
  h_toweight->SetMarkerSize(1.5);


  h_toweight_start->Scale(1./h_toweight_start->Integral());
  h_toweight_start->SetMarkerColor(kGreen);
  h_toweight_start->SetLineColor(kGreen);
  h_toweight_start->SetMarkerSize(1.5);

  TPad upperPad("upperPad","upperPad",   0., .25, 1., 1.);
  TPad lowerPad("lowerPad","lowerPad",   0., 0.,  1., .23);

  Double_t margin = 0.05;
  upperPad.SetRightMargin(margin);
  upperPad.SetLeftMargin(3*margin);
  upperPad.SetTopMargin(margin);
  lowerPad.SetRightMargin(margin);
  lowerPad.SetLeftMargin(3*margin);
  lowerPad.SetBottomMargin(2*margin);
  lowerPad.Draw();
  upperPad.Draw();

  Double_t ymax = max(h_target->GetMaximum(),h_toweight->GetMaximum());
  ymax = max(ymax,h_toweight_start->GetMaximum());
  h_target->GetYaxis()->SetRangeUser(0,ymax+0.0025);

  TLegend leg(0.75,0.80,1.00,1.00);
  leg.SetFillColor(kWhite);
  leg.SetTextSize(0.055);
  leg.SetBorderSize(0);
  leg.SetTextFont(132);
  leg.AddEntry(h_target,target_name, "pe");
  leg.AddEntry(h_toweight_start,toweight_name, "pe");
  leg.AddEntry(h_toweight,toweight_name+"| #it{w}", "pe");

  upperPad.cd();
  h_target->Draw("pe");
  h_toweight->Draw("pesame");
  h_toweight_start->Draw("pesame");
  h_target->Draw("pesame");
  h_toweight->Draw("pesame");
  h_target->Draw("pesame");
  leg.Draw("same");
  
  lowerPad.cd();
  TH1D* h_ratio = (TH1D*) h_toweight->Clone();
  h_ratio->Divide(h_target);
  h_ratio->SetLineColor(kBlue);
  h_ratio->SetFillColor(kBlue);
  h_ratio->SetMarkerColor(kBlue);
  h_ratio->SetMarkerSize(0.5);
  h_ratio->SetXTitle("");
  h_ratio->SetYTitle("");
  h_ratio->GetYaxis()->SetRangeUser(0.5,1.5);
  h_ratio->SetTitleFont(133,"Y");
  h_ratio->SetTitleSize(20,"Y");
  h_ratio->SetTitleOffset(2,"Y");
  h_ratio->SetLabelFont(133,"XY");
  h_ratio->SetLabelSize(16,"XY");
  h_ratio->SetYTitle("#frac{N("+target_name+")}{N("+toweight_name+")}");
  h_ratio->SetNdivisions(505,"Y");
  h_ratio->Draw("p");
  TF1 one("one","1",h_target->GetXaxis()->GetXmin(),h_target->GetXaxis()->GetXmax());
  one.SetLineStyle(kDashed);
  one.SetLineColor(kBlack);
  one.SetMarkerStyle(25);
  one.SetMarkerColor(kRed);
  one.Draw("psame");
  lowerPad.Update();

  Canvas->SaveAs(plotDir+variable_name+".pdf");
  Canvas->SaveAs(plotDir+variable_name+".C");
  
  return Canvas;
}

TCanvas* IDecay::compare1D(TH1D* h_target,TH1D* h_toweight,TH1D* h_toweight_start,TH1D* h_target_start,TString variable_name) {
  TCanvas* Canvas = new TCanvas(variable_name,variable_name, 50,50, 1000,700);
  
  Canvas->cd();

  h_target->Scale(1./h_target->Integral());
  h_target->SetMarkerColor(kRed);
  h_target->SetLineColor(kRed);
  h_target->SetXTitle(findAxisTitle(variable_name));
  h_target->SetMarkerSize(1.5);
  h_target->SetMarkerStyle(25);
  h_target->SetYTitle("Candidates (arbitrary scale)");

  h_toweight->Scale(1./h_toweight->Integral());
  h_toweight->SetMarkerColor(kBlue);
  h_toweight->SetLineColor(kBlue);
  h_toweight->SetMarkerSize(1.5);


  h_toweight_start->Scale(1./h_toweight_start->Integral());
  h_toweight_start->SetMarkerColor(kGreen);
  h_toweight_start->SetLineColor(kGreen);
  h_toweight_start->SetMarkerSize(1.5);

  h_target_start->Scale(1./h_target_start->Integral());
  h_target_start->SetMarkerColor(kGreen);
  h_target_start->SetLineColor(kGreen);
  h_target_start->SetMarkerSize(1.5);
  h_target_start->SetMarkerStyle(25);
 

  TPad upperPad("upperPad","upperPad",   0., .25, 1., 1.);
  TPad lowerPad("lowerPad","lowerPad",   0., 0.,  1., .23);

  Double_t margin = 0.05;
  upperPad.SetRightMargin(margin);
  upperPad.SetLeftMargin(3*margin);
  upperPad.SetTopMargin(margin);
  lowerPad.SetRightMargin(margin);
  lowerPad.SetLeftMargin(3*margin);
  lowerPad.SetBottomMargin(2*margin);
  lowerPad.Draw();
  upperPad.Draw();

  Double_t ymax = max(h_target->GetMaximum(),h_toweight->GetMaximum());
  ymax = max(ymax,h_toweight_start->GetMaximum());
  ymax = max(ymax,h_target_start->GetMaximum());
  h_target->GetYaxis()->SetRangeUser(0,ymax+0.0025);

  TLegend leg(0.75,0.75,1.00,1.00);
  leg.SetFillColor(kWhite);
  leg.SetTextSize(0.055);
  leg.SetBorderSize(0);
  leg.SetTextFont(132);
  leg.AddEntry(h_target_start,target_name, "pe");
  leg.AddEntry(h_toweight_start,toweight_name, "pe");
  leg.AddEntry(h_target,target_name+"| #it{w}", "pe");
  leg.AddEntry(h_toweight,toweight_name+"| #it{w}", "pe");

  upperPad.cd();
  h_target->Draw("pe");
  h_toweight->Draw("pesame");
  h_target_start->Draw("pesame");
  h_toweight_start->Draw("pesame");
  h_target->Draw("pesame");
  h_toweight->Draw("pesame");
  h_target->Draw("pesame");
  leg.Draw("same");
 
  lowerPad.cd();
  TH1D* h_ratio = (TH1D*) h_toweight->Clone();
  h_ratio->Divide(h_target);
  h_ratio->SetLineColor(kBlue);
  h_ratio->SetFillColor(kBlue);
  h_ratio->SetMarkerColor(kBlue);
  h_ratio->SetMarkerSize(0.5);
  h_ratio->SetXTitle("");
  h_ratio->SetYTitle("");
  h_ratio->GetYaxis()->SetRangeUser(0.5,1.5);
  h_ratio->SetTitleFont(133,"Y");
  h_ratio->SetTitleSize(20,"Y");
  h_ratio->SetTitleOffset(2,"Y");
  h_ratio->SetLabelFont(133,"XY");
  h_ratio->SetLabelSize(16,"XY");
  h_ratio->SetYTitle("#frac{N("+target_name+")}{N("+toweight_name+")}");
  h_ratio->SetNdivisions(505,"Y");
  h_ratio->Draw("p");
  TF1 one("one","1",h_target->GetXaxis()->GetXmin(),h_target->GetXaxis()->GetXmax());
  one.SetLineStyle(kDashed);
  one.SetLineColor(kBlack);
  one.SetMarkerStyle(25);
  one.SetMarkerColor(kRed);
  one.Draw("psame");
  lowerPad.Update();

  Canvas->SaveAs(plotDir+variable_name+".pdf");
  Canvas->SaveAs(plotDir+variable_name+".C");
  
  return Canvas;  
}

TCanvas* IDecay::drawResidualAsymmetry1D(TH1D* h_asym,TH1D* h_target_minus_toweight, TString variable_name) {
  
  h_asym->SetMarkerColor(kBlack);
  h_asym->SetLineColor(kBlack);
  h_asym->SetXTitle(findAxisTitle(variable_name));
  h_asym->SetMarkerSize(1.5);
  h_asym->SetYTitle("Asymmetry");
  h_asym->Fit("pol0");
  h_asym->GetYaxis()->SetRangeUser(h_asym->GetFunction("pol0")->GetParameter(0)-h_asym->GetNbinsX()*h_asym->GetFunction("pol0")->GetParError(0),h_asym->GetFunction("pol0")->GetParameter(0)+h_asym->GetNbinsX()*h_asym->GetFunction("pol0")->GetParError(0));
  h_asym->GetListOfFunctions()->Remove(h_asym->GetFunction("pol0"));
  
  h_target_minus_toweight->SetMarkerColor(kBlack);
  h_target_minus_toweight->SetLineColor(kBlack);
  h_target_minus_toweight->SetMarkerSize(1.5);
  h_target_minus_toweight->SetXTitle(findAxisTitle(variable_name));
  h_target_minus_toweight->SetYTitle("N("+target_name+")#minusN("+toweight_name+")");
 
  TF1 zero("zero","0",h_target_minus_toweight->GetXaxis()->GetXmin(),h_target_minus_toweight->GetXaxis()->GetXmax());
  zero.SetLineStyle(kDashed);
  zero.SetLineColor(kBlack);
  zero.SetMarkerStyle(25);
 
  TCanvas* Canvas = new TCanvas(variable_name+"_asym",variable_name+"_asym", 50,50, 1000,700);
  Canvas->cd();
  Canvas->Clear();
  TPad upperPad("upperPad","upperPad",   0., .55, 1., 1.);
  TPad lowerPad("lowerPad","lowerPad",   0., 0.,  1., .54);

  Double_t margin = 0.05;
  upperPad.SetRightMargin(margin);
  upperPad.SetLeftMargin(3*margin);
  upperPad.SetTopMargin(2*margin);
  upperPad.SetBottomMargin(0*margin);
  lowerPad.SetRightMargin(margin);
  lowerPad.SetLeftMargin(3*margin);
  lowerPad.SetBottomMargin(6*margin);
  lowerPad.SetTopMargin(0*margin);
  lowerPad.Draw();
  upperPad.Draw();

  // Double_t ymax = max(h_target->GetMaximum(),h_toweight->GetMaximum());
  // ymax = max(ymax,h_toweight_start->GetMaximum());
  // h_target->GetYaxis()->SetRangeUser(0,ymax+0.0025);

  // TLegend leg(0.75,0.80,1.00,1.00);
  // leg.SetFillColor(kWhite);
  // leg.SetTextSize(0.055);
  // leg.SetBorderSize(0);
  // leg.SetTextFont(132);
  // leg.AddEntry(h_target,target_name, "pe");
  // leg.AddEntry(h_toweight_start,toweight_name, "pe");
  // leg.AddEntry(h_toweight,toweight_name+"| #it{w}", "pe");

  upperPad.cd();
  h_asym->Draw("pe");
  zero.Draw("lsame");
  upperPad.Update();
  
  lowerPad.cd();
  h_target_minus_toweight->Draw("pe");
  zero.Draw("lsame");
  lowerPad.Update();

  Canvas->SaveAs(plotDir+variable_name+"_asym.pdf");
  Canvas->SaveAs(plotDir+variable_name+"_asym.C");
  
  return Canvas;
}

void IDecay::showVariables() {
  
  size_t Nvars;
  TH1D h1D_target;
  TH1D h1D_target_start;
  TH1D h1D_toweight;
  TH1D h1D_toweight_start;
  TH2D h2D_target;
  TH2D h2D_target_start;
  TH2D h2D_toweight;
  TH2D h2D_toweight_start;
  TH3D h3D_target;
  TH3D h3D_target_start;
  TH3D h3D_toweight;
  TH3D h3D_toweight_start;
  
  myH tmp, Asym_tmp, tmp_A, tmp_B;
  double // A_average, 
    DeltaA, DeltaAerr;
  TH1D h1D_asym;
  TH1D h1D_target_minus_toweight;;
  
  toweight_Hs.clear();     
  toweight_Hs_asym.clear();     
  for (size_t i_target = 0; i_target < target_decays.size(); i_target++) {
    target_name = target_decays.at(i_target)->title;
    toweight_name = title;
    Nvars = variables.at(i_target).size();
  

    for (size_t i_var = 0; i_var < Nvars; i_var++) {
      cout << variables.at(i_target).at(i_var) << '\t';
    }
    cout << endl;
    
    //in the case also the target has been Iweighted
    if (target_decays.at(i_target)->Iw.size() != 0) {
      if (!calculateResidualAsymmetry) {
	target_Hs.at(i_target) = fillmyH(i_target,1);
      }
      else {
	target_Hs_asym.at(i_target) = fillmyH(i_target,/*bool isTarget=*/1,/*bool isPositive=*/1);
	tmp = fillmyH(i_target,/*bool isTarget=*/1,/*bool isPositive=*/0);
	target_Hs.at(i_target) = target_Hs_asym.at(i_target);
	target_Hs.at(i_target).add(&tmp);
	target_Hs_asym.at(i_target).getasymmetry(&tmp);
      }
    }

    if (!calculateResidualAsymmetry) {
      toweight_Hs.push_back(fillmyH(i_target,0));
    }
    else {
      toweight_Hs_asym.push_back(fillmyH(i_target,/*bool isTarget=*/0,/*bool isPositive=*/1));
      tmp = fillmyH(i_target,/*bool isTarget=*/0,/*bool isPositive=*/0);
      toweight_Hs.push_back(toweight_Hs_asym.at(i_target));
      toweight_Hs.at(i_target).add(&tmp);
      toweight_Hs_asym.at(i_target).getasymmetry(&tmp);
    }


    if(calculateResidualAsymmetry) {
      tmp_A = target_Hs.at(i_target);
      tmp_A.norm();
      tmp_B = toweight_Hs.at(i_target);
      tmp_B.norm();
    
      //cout << "chi2/Ndof of Asymmetry distribution: ";
      //cout << target_Hs_asym.at(i_target).getChi2(&toweight_Hs_asym.at(i_target))/target_Hs.at(i_target).Nbins_tot << endl;
      
      Asym_tmp = Nevts > target_decays.at(i_target)->Nevts ? toweight_Hs_asym.at(i_target) : target_Hs_asym.at(i_target);
      if (Nvars == 1) {
      	h1D_asym = Asym_tmp.convert2TH1D();
      	h1D_asym.SetName("h1D_asym");
      	h1D_asym.SetTitle("h1D_asym");
      }
      tmp = tmp_A;
      tmp.add(&tmp_B,-1);
      //tmp.zeroErrors(); //negligible uncertainty but we take it anyway
      if (Nvars == 1) {
      	h1D_target_minus_toweight = tmp.convert2TH1D();
      	h1D_target_minus_toweight.SetName("h1D_target_minus_toweight");
      	h1D_target_minus_toweight.SetTitle("h1D_target_minus_toweight");
      }
      tmp.multiply(&Asym_tmp);
      DeltaA = tmp.integral(DeltaAerr);
	
      for (size_t i_var = 0; i_var < Nvars; i_var++) {
      	cout << variables.at(i_target).at(i_var) << '\t';
      }
      cout << "DeltaA: ";
      cout << DeltaA;
      cout << '\t' << "DeltaA_err: ";
      cout << DeltaAerr << endl;
  
      /*
      myH Asym_A = target_Hs_asym.at(i_target);
      myH Asym_B = toweight_Hs_asym.at(i_target);
     
      double DeltaA_A, DeltaAerr_A;
      tmp = tmp_A;
      tmp.add(&tmp_B,-1);
      tmp.zeroErrors(); //negligible uncertainty
      tmp.multiply(&Asym_A);
      DeltaA_A = tmp.integral(DeltaAerr_A);
      
      double DeltaA_B, DeltaAerr_B;
      tmp = tmp_B;
      tmp.add(&tmp_A,-1);
      tmp.zeroErrors(); //negligible uncertainty
      tmp.multiply(&Asym_B);
      DeltaA_B = tmp.integral(DeltaAerr_B);

      DeltaA = (DeltaA_A - DeltaA_B)/2; 
      DeltaAerr = sqrt(DeltaAerr_A*DeltaAerr_A+DeltaAerr_B*DeltaAerr_B);
      
      for (size_t i_var = 0; i_var < Nvars; i_var++) {
	cout << variables.at(i_target).at(i_var) << '\t';
      }
      cout << "DeltaA: ";
      cout << DeltaA;
      cout << '\t' << "DeltaAerr: ";
      cout << DeltaAerr << endl;
      */
    }

        
    if (Nvars == 3) {
      h3D_target = target_Hs.at(i_target).convert2TH3D();
      h3D_target.SetName("h3D_target");
      h3D_target.SetTitle("h3D_target");
      h3D_target_start = target_Hs_start.at(i_target).convert2TH3D();
      h3D_target_start.SetName("h3D_target_start");
      h3D_target_start.SetTitle("h3D_target_start");
      h3D_toweight = toweight_Hs.at(i_target).convert2TH3D();
      h3D_toweight.SetName("h3D_toweight");
      h3D_toweight.SetTitle("h3D_toweight");
      h3D_toweight_start = toweight_Hs_start.at(i_target).convert2TH3D();
      h3D_toweight_start.SetName("h3D_toweight_start");
      h3D_toweight_start.SetTitle("h3D_toweight_start");
  	
      if (target_decays.at(i_target)->Iw.size() != 0) {
	compare1D(h3D_target.ProjectionX(),h3D_toweight.ProjectionX(),h3D_toweight_start.ProjectionX(),h3D_target_start.ProjectionX(),variables.at(i_target).at(0));
	compare1D(h3D_target.ProjectionY(),h3D_toweight.ProjectionY(),h3D_toweight_start.ProjectionY(),h3D_target_start.ProjectionY(),variables.at(i_target).at(1));
	compare1D(h3D_target.ProjectionZ(),h3D_toweight.ProjectionZ(),h3D_toweight_start.ProjectionZ(),h3D_target_start.ProjectionZ(),variables.at(i_target).at(2));
      }
      else {
	compare1D(h3D_target.ProjectionX(),h3D_toweight.ProjectionX(),h3D_toweight_start.ProjectionX(),variables.at(i_target).at(0));
	compare1D(h3D_target.ProjectionY(),h3D_toweight.ProjectionY(),h3D_toweight_start.ProjectionY(),variables.at(i_target).at(1));
	compare1D(h3D_target.ProjectionZ(),h3D_toweight.ProjectionZ(),h3D_toweight_start.ProjectionZ(),variables.at(i_target).at(2));
      }

    }
    else  if (Nvars == 2) {
      h2D_target = target_Hs.at(i_target).convert2TH2D();
      h2D_target.SetName("h2D_target");
      h2D_target.SetTitle("h2D_target");
      h2D_target_start = target_Hs_start.at(i_target).convert2TH2D();
      h2D_target_start.SetName("h2D_target_start");
      h2D_target_start.SetTitle("h2D_target_start");
      h2D_toweight = toweight_Hs.at(i_target).convert2TH2D();
      h2D_toweight.SetName("h2D_toweight");
      h2D_toweight.SetTitle("h2D_toweight");
      h2D_toweight_start = toweight_Hs_start.at(i_target).convert2TH2D();
      h2D_toweight_start.SetName("h2D_toweight_start");
      h2D_toweight_start.SetTitle("h2D_toweight_start");
  
      if (target_decays.at(i_target)->Iw.size() != 0) {
	compare1D(h2D_target.ProjectionX(),h2D_toweight.ProjectionX(),h2D_toweight_start.ProjectionX(),h2D_target_start.ProjectionX(),variables.at(i_target).at(0));
	compare1D(h2D_target.ProjectionY(),h2D_toweight.ProjectionY(),h2D_toweight_start.ProjectionY(),h2D_target_start.ProjectionY(),variables.at(i_target).at(1));
      }
      else {
	compare1D(h2D_target.ProjectionX(),h2D_toweight.ProjectionX(),h2D_toweight_start.ProjectionX(),variables.at(i_target).at(0));
	compare1D(h2D_target.ProjectionY(),h2D_toweight.ProjectionY(),h2D_toweight_start.ProjectionY(),variables.at(i_target).at(1));
      }
    
    }
    else if (Nvars == 1) {
      h1D_target = target_Hs.at(i_target).convert2TH1D();
      h1D_target.SetName("h1D_target");
      h1D_target.SetTitle("h1D_target");
      h1D_target_start = target_Hs_start.at(i_target).convert2TH1D();
      h1D_target_start.SetName("h1D_target_start");
      h1D_target_start.SetTitle("h1D_target_start");
      h1D_toweight = toweight_Hs.at(i_target).convert2TH1D();
      h1D_toweight.SetName("h1D_toweight");
      h1D_toweight.SetTitle("h1D_toweight");
      h1D_toweight_start = toweight_Hs_start.at(i_target).convert2TH1D();
      h1D_toweight_start.SetName("h1D_toweight_start");
      h1D_toweight_start.SetTitle("h1D_toweight_start");
  

      if (target_decays.at(i_target)->Iw.size() != 0)
	compare1D(&h1D_target,&h1D_toweight,&h1D_toweight_start,&h1D_target_start,variables.at(i_target).at(0));
      else
	compare1D(&h1D_target,&h1D_toweight,&h1D_toweight_start,variables.at(i_target).at(0));


      if(calculateResidualAsymmetry) {
	drawResidualAsymmetry1D(&h1D_asym,&h1D_target_minus_toweight,variables.at(i_target).at(0));
      }   
    }
   
  }
  
}
void IDecay::setIWeight(string iweight) {
 
  Iw.clear();
  TLeaf* leaf;
  vector < double > tmp_Iw;
  for (size_t i_evt = Nevts_start; i_evt < Nevts_start+Nevts; i_evt++) {
    leaf = (TLeaf*) tree->GetLeaf((const char*)iweight.c_str());
    leaf->GetBranch()->GetEntry(i_evt);
    tmp_Iw.push_back(leaf->GetValue());
  }

  Iw.push_back(tmp_Iw);

}

//SetAsymmetryVariable(string asymvariable) {
//loop between the several different decays->SetAsymvariable(asymvariable)
//calculateResAsym=true <- variable for fillTargets and ShowVariables
//};

void IDecay::fillTargets(){
  
  myH tmp_minus;

  target_Hs.clear();
  toweight_Hs_start.clear();
  target_Hs_asym.clear();
  if (!calculateResidualAsymmetry) {
    for (size_t i_target = 0; i_target < target_decays.size(); i_target++) {
      target_Hs.push_back(fillmyH(i_target,1));
      toweight_Hs_start.push_back(fillmyH(i_target,0));
    }
  }
  else {
    for (size_t i_target = 0; i_target < target_decays.size(); i_target++) {
      target_Hs_asym.push_back(fillmyH(i_target,/*bool isTarget=*/1,/*bool isPositive=*/1));
      tmp_minus = fillmyH(i_target,/*bool isTarget=*/1,/*bool isPositive=*/0);
      target_Hs.push_back(target_Hs_asym.at(i_target));
      target_Hs.at(i_target).add(&tmp_minus);
      target_Hs_asym.at(i_target).getasymmetry(&tmp_minus);
      
      toweight_Hs_start.push_back(fillmyH(i_target,/*bool isTarget=*/0,/*bool isPositive=*/1));
      tmp_minus = fillmyH(i_target,/*bool isTarget=*/0,/*bool isPositive=*/0);
      toweight_Hs_start.at(i_target).add(&tmp_minus);
    }
  }

  target_Hs_start = target_Hs;
}

void IDecay::getStats(){
  vector < vector < double > > Iw = this->Iw;
  size_t Nevts = this->Nevts;

  double tmp_Iw = 1;
  double sum_Iw = 0;
  double sum_Iw2 = 0;
  for (size_t i_evt = 0; i_evt < Nevts; i_evt++) {
        
    tmp_Iw = 1;
    for (size_t i_tar = 0; i_tar < Iw.size()/*Ntargets*/; i_tar++) {
      tmp_Iw *= Iw.at(i_tar).at(i_evt);
    }
    
    if (w.size() != 0)
      tmp_Iw*=w.at(i_evt) > 0; 

    sum_Iw += tmp_Iw;
    sum_Iw2 += tmp_Iw*tmp_Iw;
  }

  double N_effective = sum_Iw*sum_Iw/sum_Iw2;
  double N_effective_O_N_original = N_effective/Nevts;


  cout << "#weighting statistics for " << name << endl;
  cout << "Noriginal = " << Nevts << endl;
  cout << "Neffective = " << N_effective << endl;
  cout << "StatisticalPowerReduction = " << N_effective_O_N_original << endl;

  // cout << "N_{original}" << '\t' << "&" << '\t' << "sum_w" << '\t' << "&" << '\t' << "sum_w2" << '\t' << "&" << '\t' << "N_{effective}" << '\t' << "&" << '\t' << "N_{effective}/N_{original}" << endl;

  // cout << N_original << '\t' << "&" << '\t' << sum_w << '\t' << "&" << '\t' << sum_w2 << '\t' << "&" << '\t' << N_effective << '\t' << "&" << '\t' << N_effective_O_N_original << endl;

  
}
